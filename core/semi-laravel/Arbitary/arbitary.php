<?php

$theme = null;

function asset( $address )
{
	global $theme;

	if( $theme == null )
	{
        $theme = get_template_directory_uri();
        
        return $theme . "/" . $address;
	}
    
    else   
    {
        return $theme . "/" . $address; 
    }
}

function match( $modelMatch , $modelDataCollection )
{
    $exchange = new stdClass();

    foreach($modelMatch as $column => $formitem)
    {
        $exchange->{$formitem} = $modelDataCollection->{$column};
    }

    return $exchange;
}
