<?php namespace Wp\Menu\Core;

class Formatter
{
    public $Native = null;

    public $Sort = null;

    public function __construct()
    {
        $this->WpNativeFormatterInstance();

        $this->SortInstance();
    }
    
    public function raw()
    {
        $param = array(
          'container'       => false,
          'echo'            => false,
          'items_wrap'      => '%3$s',
          'depth'           => 0,
          'walker'          => $this->Native
        );

        return strip_tags(wp_nav_menu( $param ));
    }
        
    
    public function format( $args )
    {

        $this->Native->defaults = array_merge( $this->Native->defaults , $args );

        if(isset($this->Native->defaults['attr']))
            $this->Native->setItemAttribute( $this->Native->defaults['attr'] );
        
        $param = array(
          'container'       => false,
          'echo'            => false,
          'items_wrap'      => '%3$s',
          'depth'           => 0,
          'walker'          =>  $this->Native
        );
       
        return wp_nav_menu( $param );
    }

    public function intoArray()
    {
        $param = array(
            'container'       => false,
            'echo'            => false,
            'items_wrap'      => '%3$s',
            'depth'           => 0,
            'walker'          => $this->Native
        );

        wp_nav_menu( $param );

        $parentsUp = $this->Sort->sort( $this->Native->items );

        return $tree;
    }


    
    public function WpNativeFormatterInstance()
    {
        if( $this->Native == null )
        {
            $this->Native = new \Wp\Menu\Core\WpNativeFormatter;
            
            return $this->Native;
        }
        
        else
        {
            return $this->Native;
        }
    }

    public function SortInstance()
    {
        if( $this->Sort == null )
        {
            $this->Sort = new \Wp\Menu\Core\Sort;

            return $this->Sort;
        }

        else
        {
            return $this->Sort;
        }
    }
    
}