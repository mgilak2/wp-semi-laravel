<?php namespace Wp\Menu\Core;

class Sort
{
    public $items;

    public function sort( $items )
    {
        $this->items = $items;

        $this->setParent();

        $this->setSibling();

        $this->parentsUp();

        $this->merge();

        return array(
            'mainParent'=>$this->parents,
            'parentsUp'=>$this->parentsUp
        );
    }

    public function maxDepth()
    {
        $max = 0;

        foreach($this->items as $item)
        {
            if($item['depth']>$max)
            {
                $max = $item['depth'];
            }
        }

        return $max;
    }

    public function merge()
    {
        foreach($this->parentsUp as $indexUp => $parentUp)
        {
            foreach($parentUp as $parent)
            {
                foreach($parent as $index => $item)
                {
                    if(isset($this->parentsUp[$item]))
                    {
                        $this->parentsUp[$indexUp][0][$index] = array($item=>$this->parentsUp[$item]);
                    }
                }
            }
        }
    }

    public function parentsUp()
    {
        foreach($this->parents as $parent)
        {
            $this->parentsUp[$parent[0]] = array();
        }

        foreach($this->children as $child)
        {
            $this->parentsUp[($this->items[$child[0]]['parent'])][] = $child;
        }
    }

    public function hasParent($item)
    {
        if($item['parent'] !== null)
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    public function setSibling()
    {
        for($i=0; $i<count($this->items); $i++)
        {
            $index = count($this->items) -1 -$i;

            $this->currentIndex = $index;

            $sibling = array($this->currentIndex);

            $this->sibling($index,$sibling);

            if(isset($this->items[$index]['parent']))
                $this->children[] = $this->siblingResult['sibling'];

            if(! isset($this->items[$index]['parent']))
            {
                $this->parents[] = $this->siblingResult['sibling'];
            }

            $i = count($this->items) - $this->siblingResult['index'] -1;
        }
    }

    public function sibling($index,$sibling = array(),$i = 1)
    {
        if(
            $this->items[$index -1]['parent'] == $this->items[$index]['parent']
            && isset($this->items[$index]['parent'])
        )
        {
            $sibling[] = $index -1;

            $this->sibling($index -$i,$sibling,$i++);
        }

        else
        {
            $this->siblingResult = array(
                'sibling' =>$sibling,
                'index'   =>$index
            );
        }
    }

    public function hasSibling($items , $index)
    {
        if(isset($items[$index - 1]))
        {
            if($items[$index]['parent'] == $items[$index - 1]['parent'])
            {
                return true;
            }

            else
            {
                return false;
            }
        }
    }

    public function setParent()
    {
        for($i=0;$i<count($this->items);$i++)
        {
            $depth = $this->items[count($this->items) -1 -$i]['depth'];

            $index = count($this->items) -1 -$i;

            $this->parent($depth,$index);
        }
    }

    public function parent( $depth , $index ,$i = 1)
    {
        if($depth > 0)
        {
            if ( $this->items[$index - $i]['depth'] != $depth )
            {
                $this->items[$index]['parent'] = $index - $i;
            }

            else
            {
                $i++;

                $this->parent($depth , $index , $i);
            }
        }
    }

}