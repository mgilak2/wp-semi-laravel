<?php namespace Wp\Menu\Core;

class Core
{
  
    public static $formatter = null;
    
    public static function register( $name = "" , $desc = "" )
    {     
        register_nav_menu( $name , $desc );
    }
    
    public static function unregister( $name )
    {
        add_action('after_setup_theme',function() use ($name) {
            
            unregister_nav_menu( $name );
            
        },11);           
    }
    
    public static function get( $name = "primary" , $args = array() , $func = null )
    {
        if( count($args) > 0 )
        {
            $args['menu'] = $name;

            $formatter = self::FormatterInstance();

            return $formatter->format( $args );
        }
        
        else
        {
            return wp_nav_menu( $name ); 
        }
    }

    public static function intoArray()
    {
        $formatter = self::FormatterInstance();

        return $formatter->intoArray();
    }

    public static function FormatterInstance()
    {
        if( self::$formatter == null )
        {
            self::$formatter = new \Wp\Menu\Core\Formatter;
            
            return self::$formatter;
        }
        
        else
        {
            return self::$formatter;   
        }
        
    }
    
    
}