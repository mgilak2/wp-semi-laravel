<?php namespace Wp\Menu\Core;

class WpNativeFormatter extends \Walker {

    
    /**
	 * What the class handles.
	 *
	 * @see Walker::$tree_type
	 * @since 3.0.0
	 * @var string
	 */
	var $tree_type = array( 'post_type', 'taxonomy', 'custom' );
    
	/**
	 * Database fields to use.
	 *
	 * @see Walker::$db_fields
	 * @since 3.0.0
	 * @todo Decouple this.
	 * @var array
	 */
	var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );
    
    
    public $defaults = array(
        "before"                        => "<li>",
        "after"                         => "</li>",
        "item"                          => array("open" => "<a>","close"=> "</a>"),
        'sub-menu'                      => array( "open" => "<ul class=\"sub-menu\">", "close" => "</ul>"),
        "sub-menu-indent"               => array('open'=>"","close"=> ""),
        'attr'                          => null,
        'menu'                          => 'primary'
    );
    
    
    /**
    *
    * Holding functions . i use this style to set new functionality 
    * some times
    *
    */
    public $subMenuindent = null,
           $elementindent = null,
           $itemTag = null;

    public $itemCounter = 0;
    
    public function __construct( $args = array() )
    {
        $this->defaults = array_merge( $this->defaults , $args );    
    }
    
    
    
    public function setSubMenuindent( $fnc )
    {
        $this->indent = $fnc;
    }

    public function subMenuindent($depth)
    {
        if( $this->subMenuindent == null )
        {
            return  str_repeat("\t", $depth);
        }

        else
        {
            return $this->subMenuindent( $depth );
        }
    }
    
	/**
	 * Starts the list before the elements are added.
	 *
	 * @see Walker::start_lvl()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) 
    {
		$indent = $this->subMenuindent( $depth );
        
		$output .= $this->defaults['sub-menu-indent']['open']
            . $indent 
            . $this->defaults['sub-menu']['open'];
	}

    
    
    
	/**
	 * Ends the list of after the elements are added.
	 *
	 * @see Walker::end_lvl()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = $this->subMenuindent( $depth );
        
		$output .= $this->defaults['sub-menu-indent']['close']
            . $indent 
            . $this->defaults['sub-menu']['close'];
	}
    

    
    public function setElementindent( $fnc )
    {
        $this->elementindent = $fnc;
    }
    
    
    public function elementindent( $depth )
    {
        if($this->elementindent == null)
        {
            return ( $depth ) ? str_repeat( "\t", $depth ) : '';
        }
        
        else
        {
            return $this->elementindent( $depth );
        }
            
    }
        

    /**
     * Filter the CSS class(es) applied to a menu item's <li>.
     *
     * @since 3.0.0
     *
     * @see wp_nav_menu()
     *
     * @param $item
     * @param $args
     */
    private function setClass( $item , $args )
    {
        $class_names = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		
        $classes[] = 'menu-item-' . $item->ID;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $this->classes = $class_names;
    }
    
    
    
    
    /**
     * Filter the ID applied to a menu item's <li>.
     *
     * @since 3.0.1
     *
     * @see wp_nav_menu()
     */
    private function setId($item , $args)
    {
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
        
        $this->Id = $id;
    }
    
    
    
    
    
    /**
    *
    * sets anchor tag attributes
    * 
    * @param $item
    */
    private function setAttribute( $item )
    {
        $atts = array();
        
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        
        $this->title = $atts['title'];
        
		$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        
        $this->target = $atts['target'];
        
		$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        
        $this->rel = $atts['rel'];
        
		$atts['href']   = ! empty( $item->url )        ? $item->url        : '';
        
        $this->href = $atts['href'];

        if( is_array($this->atts) )
        {
            $this->atts = array_merge( $this->atts , $atts );
        }
        
        else
        {
            $this->atts = $atts;
        }
        
    }

    
    
    
    
    /**
    * Filter the HTML attributes applied to a menu item's <a>.
    *
    * @since 3.6.0
    *
    * @see wp_nav_menu()
    * @param object $item The current menu item.
    * @param array  $args An array of wp_nav_menu() arguments.
    */
    private function filterAttribute( $item , $args )
    {
		$atts = apply_filters( 'nav_menu_link_attributes', $this->atts, $item, $args );

		$attributes = '';
        
        if( $this->defaults['item']['open'] !== "<a>" )
        {
            $this->atts['href'] = '';
        }
        
		foreach ( $this->atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}
        
        $this->Attributes = $attributes;
    }
    
    
    
    
    /** This filter is documented in wp-includes/post-template.php 
    *
    *
    * @param $item
    *
    * @param $args
    */
    private function item( $item , $args )
    {
        if( $this->itemTag == null )
        {
            $item_output = $args->before;
            
            $before = $this->defaults['item']['open'];
        
            $openBefore = trim(substr($before,0,-1));

            $item_output .= $openBefore . $this->Attributes .'>';
            
            $itemName = $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

            $item_output .= $itemName;

            $item_output .= $this->defaults['item']['close'];

            $item_output .= $args->after;

            $this->item_output = $item_output;
        }
        
        else
        {
                $this->itemTag();
        }
       
    }
    
    public function setItemTag( $open , $close = "" )
    {
       $this->defaults['itemTag'] = array("open" => $open , "close" => $close);
    }
    
    
    public function setItemAttribute( array $args )
    {
        if( $args != null )
        {

            if( $this->defaults['itemTag']['open'] == "<a>" )
            {
                $this->atts['href'] = "";
            }

            foreach( $args as $att => $val )
            {
                $this->atts[$att] = $val;   
            }
            
        }
        
    }



    /**
     * Filter a menu item's starting output.
     *
     * The menu item's starting output only includes $args->before, the opening <a>,
     * the menu item's title, the closing </a>, and $args->after. Currently, there is
     * no filter for modifying the opening and closing <li> for a menu item.
     *
     * @since 3.0.0
     *
     * @see wp_nav_menu()
     *
     * @param $item
     * @param $depth
     * @param $args
     * @return mixed|void
     */
    private function filterMenuItem( $item , $depth , $args )
    {
        return apply_filters( 
            'walker_nav_menu_start_el',
            $this->item_output,
            $item,
            $depth,
            $args 
        );        
    }
    
    
    
    
	/**
	 * Start the element output.
	 *
	 * @see Walker::start_el()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Menu item data object.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 * @param int    $id     Current item ID.
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) 
    {
		$indent = $this->elementindent( $depth );

        $this->setClass( $item , $args );	
        
        $this->setId($item , $args);
        
        $before = $this->defaults['before'];
        
        $openBefore = trim(substr($before,0,-1));
            
		$output .= $indent . $openBefore . $this->Id . $this->classes .'>';

		$this->setAttribute( $item );
        
        $this->filterAttribute( $item , $args );

        $this->item( $item , $args );

        $this->items[] = [
            'output'  => $this->item_output,
            'depth'   => $depth
        ];

        $this->itemCounter++;

		$output .= $this->filterMenuItem( $item , $depth , $args );
	}

	/**
	 * Ends the element output, if needed.
	 *
	 * @see Walker::end_el()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Page data object. Not used.
	 * @param int    $depth  Depth of page. Not Used.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() )
    {
		$output .= $this->defaults['after'];
	}

} // Walker_Nav_Menu
