<?php
/**
 * including dependency of Finder.php
 */
include __DIR__ . '/../../../../app/config/Config.php';
use App\Config\Config as Config;

include __DIR__ . '/../../Finder.php';
use Wp\View\Finder as Finder;

class FinderTest extends PHPUnit_Framework_TestCase
{

    public static $finder;

    public static function setUpBeforeClass()
    {
        self::$finder = New Finder;

        self::$finder->viewsDirection = __DIR__;
    }

    /**
     * as finder function has a dependency on Finder::$viewsDirection
     * and with the fact that this value get fills in constructor
     * we can change it in testFinder function to this FinderTest direction
     * so we can have our view files in here
     * i already add myview.php
     * view.php
     * view.blade.php
     * to this directory
     */
    public function testFinderNotBlade()
    {
        $this->assertEquals(__DIR__.'/myview.php',self::$finder->find('myview')['path']);

        $this->assertFalse(self::$finder->find('myview')['isBlade']);
    }

    /**
     * due to constructor of Blade package
     * we have to pass it views direction
     * and then this package needs only
     * the name of a blade file to work the rest
     * isBlade = true shows us we are in the write else if statement
     * of a finder function
     */
    public function testFinderBlade()
    {
        $this->assertEquals('view',self::$finder->find('view')['path']);

        $this->assertEquals(true,self::$finder->find('view')['isBlade']);
    }

    /**
     *
     */
    public function testNothingFound()
    {
        $this->assertNotEquals('notExistedView',self::$finder->find('notExistedView'));
    }

}