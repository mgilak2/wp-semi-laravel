<?php

include 'vendor/autoload.php';
include __DIR__ . '/../../../../app/config/Config.php';
include __DIR__ . '/../../Finder.php';

include '../../View.php';
use Wp\View\View as View;

class ViewTest extends PHPUnit_Framework_TestCase
{

    public function testMakeBlade()
    {
        $this->expectOutputString('view.blade');

        View::make("view",[],__DIR__);
    }

    public function testMakeNotBlade()
    {
        $this->expectOutputString('other view');

        View::make("otherView",[],__DIR__);
    }

    public function testGetInstance()
    {
        @View::getInstance();

        $this->assertInstanceOf('Wp\View\View',View::$view);
    }

    public function testGetInstanceWithArg()
    {
        @View::getInstance(__DIR__);

        $this->assertEquals(__DIR__,View::$view->finder->viewsDirection);
    }

    public function testDisplayBlade()
    {
        $this->expectOutputString('view.blade');

        $finder = new \Wp\View\Finder();
        $finder->viewsDirection = __DIR__;

        $view = new Wp\View\View( $finder );
        $view->bladeCache = __DIR__;

        $view->display(['path'=>'view','isBlade'=>true],[]);
    }

    public function testDisplayNotBlade()
    {
        $this->expectOutputString('other view');

        $finder = new \Wp\View\Finder();
        $finder->viewsDirection = __DIR__;

        $view = new Wp\View\View( $finder );

        $view->display(['path'=>__DIR__.'/otherView.php','isBlade'=>false],[]);
    }

    public function testDisplayWithArgBlade()
    {
        $this->expectOutputString('hasan gilak');

        $finder = new \Wp\View\Finder();
        $finder->viewsDirection = __DIR__;

        $view = new Wp\View\View( $finder );
        $view->bladeCache = __DIR__;

        $view->display(['path'=>'args','isBlade'=>true],['name'=>'hasan gilak']);
    }

    public function testDisplayWithArgNotBlade()
    {
        $this->expectOutputString('hasan gilak');

        $finder = new \Wp\View\Finder();
        $finder->viewsDirection = __DIR__;

        $view = new Wp\View\View( $finder );

        $view->display(['path'=>__DIR__.'/arg.php','isBlade'=>false],['name'=>'hasan gilak']);
    }

}