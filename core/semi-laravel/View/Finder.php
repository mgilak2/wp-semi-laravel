<?php namespace Wp\View;

class Finder
{
    /**
     * @var string store the direction of our views added for handling dependencies on \App\Config\Config to
     */
    public $viewsDirection ;

    /**
     * mainly handle the dependency on \App\Config\Config and also
     * lets the class to be more testable on direction
     * in this style we can change the direction to any other place
     */
    public function __construct()
    {
        if(! isset($this->viewsDirection))
            $this->viewsDirection = \App\Config\Config::view();
    }

    /**
     * depends on name of the file
     * if you path view first it checks
     * the blade version
     * if it not exist it checks for none blade
     * and at the else it fills the path with noFileFound + $name
     *
     * in blade path fills with only the name not the full address
     * but in the not balde one it returns the whole address
     *
     * @param $name
     * @return array
     */
    public function find( $name )
    {

        if( file_exists ( $this->viewsDirection . "/" . $name . ".blade.php") )
        {            
            return array( "path" => $name , "isBlade" => true );
        }
        
        elseif( file_exists ( $this->viewsDirection . "/" . $name . ".php") )
        {
            return array( "path" => $this->viewsDirection . "/" . $name . ".php" , "isBlade" => false );
        }
        
        else 
        {
            return array( "path" => "noFileFounded + $name" , "isBlade" => false );
        }
        
    }
    
}