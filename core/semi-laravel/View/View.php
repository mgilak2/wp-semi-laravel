<?php namespace Wp\View;

class View
{
    /**
     * @var null handle the singleton
     */
    public static $view = null;

    /**
     * @var holds a dependency on Wp\View\Finder
     */
    public $finder ;

    /**
     * resolve dependency on Wp\View\Finder
     * @param $finder
     */
    public function __construct( $finder )
    {
        $this->finder = $finder;
    }

    /**
     * simply make a View object trough self::getInstance
     * and display out the result
     *
     * @param $name
     * @param array $data
     * @param null $baseDir can be use to give another direction for current view
     */
    public static function make( $name , $data = array() , $baseDir = null )
    {
        self::getInstance( $baseDir );
        
        $pathArray = self::$view->finder->find( $name );
        
        self::$view->display( $pathArray , $data );
    }

    /**
     * for singleton design pattern on View object
     * also there is some flexibility over changing views direction
     * with passing your custom direction to self::make as third parameter
     *
     * @param null $baseDir
     * @return null
     */
    public static function getInstance( $baseDir = null )
    {
        if( self::$view == null )
        {
            $finder = new \Wp\View\Finder();

            if($baseDir)
                $finder->viewsDirection = $baseDir;

            self::$view = new View( $finder );

            self::$view->bladeCache = \App\Config\Config::bladeCache();
        }
        
        else
        {
            return self::$view;
        }
    }

    /**
     * this is where all the other functions outputs
     * make a use . according to isBlade it understands
     * whether it must deal with the blade or not
     *
     * @param array $pathArray
     * @param $data
     */
    public function display( array $pathArray , $data )
    {   
        
        if( $pathArray['isBlade'] )
        {
            $blade = new \Philo\Blade\Blade( $this->finder->viewsDirection , $this->bladeCache );
            
            echo $blade->view()->make( $pathArray['path'] , $data);
        }
        
        else
        {
            foreach( $data as $name => $value )
            {
               $$name = $value;  
            }
            
            include $pathArray['path'];
        }
    }
      
    
}