<?php namespace Wp\Theme\Core;

class Setting
{

    public function register( $page )
    {
        
        $validate_options = $this->validateOptions();
                
        $themeOptions = $this->themeOption( $page );
        
        function themeOptions()
        {
            $themeOptions();
        }
        
        function validate_options()
        {
            $validate_options();
        }
        
        \Action::raw( 'admin_init' , function()
        {
            register_setting(
                'themeOptions',
                'soptions',
                'validate_options'
            );
        });
        
    }
    
    private function validateOptions()
    {
        return function( $input )
        {
            $input['footer_copyright'] = wp_filter_nohtml_kses( $input['footer_copyright'] );

            return $input;
        };
    }
    
    private function themeOption( $page )
    {
        $themeOptions = function() use ($page)
        {
            add_theme_page('Theme Options',
                           'تنظیمات قالب',
                           'edit_theme_options',
                           'theme_options',
                           $page
            );
        };
        
        \Action::raw('admin_menu',$themeOptions);
        
        return $themeOptions;
    }
    
    public function view()
    {
        
    }
    
    public function validation()
    {
        
    }
    
}