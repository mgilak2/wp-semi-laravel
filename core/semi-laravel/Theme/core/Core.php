<?php namespace Wp\Theme\Core;

use Action;
use Database;

class Core
{
    public static $setting = null;
    
    public static function create( $name , $func )
    {
        Action::raw("after_switch_theme",function() use( $name , $func ){
            Database::table( $name , $func );
        },10);
    }

    public static function table( $name , $func )
    {
        Action::raw("after_switch_theme",function() use( $name , $func ){
            Database::schema()->table( $name , $func );
        },10);
    }
    
    public static function seed( $model , $args = array() , $func = null )
    {
        Action::raw("after_switch_theme",function() use($model , $args , $func)
        {
            Database::seed( $model , $args , $func );
        },10);
    }
    
    public static function setting( $page ){

        if ( is_admin() )
        {
            $setting = self::settingInstance();
         
            $setting->register( $page );
        }
    
    }
    
    public static function settingInstance()
    {
        if(self::$setting == null)
        {
            self::$setting = new \Wp\Theme\Core\Setting;
            
            return self::$setting; 
        }
        
        else
        {
            return self::$setting ;
        }
    }
    
}