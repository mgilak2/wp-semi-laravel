<?php namespace Wp\User\Core;

class UserMeta extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "wp-usermeta";
    protected $primaryKey = "umeta_id";
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('UserModel');
    }

}