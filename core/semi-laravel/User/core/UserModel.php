<?php namespace Wp\User\Core;

class UserModel extends \Illuminate\Database\Eloquent\Model
{
    protected $table="wp-users";
    protected $primaryKey = "ID";
    public $timestamps = false;

    public static function pass()
    {

    }

    public static function username()
    {

    }

    public static function email()
    {

    }

    public static function nicename()
    {

    }

    public function metas()
    {
        return $this->hasMany('UserMeta');
    }

    public static function find()
    {

    }

    public static function findUserById($id)
    {
        return get_user_by( 'ID', $id );
    }

    public static function findByUsername($username)
    {
        return get_user_by( 'login', $username );
    }
    
    public static function findByEmail($email)
    {
        return get_user_by( 'email', $email );
    }
    
    public static function findByNiceName()
    {
        
    }
}