<?php namespace Wp\User\Core;

class UserCore extends UserModel
{
    public static $username;
    public static $email;

    protected $firstName;
    protected $lastName;
    protected $role;

    /**
     * creates a user object
     *
     * @param array $info
     */
    public function __construct(array $info)
    {

    }

    /**
     * @return string
     */
    public function firstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function lastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * register users by email and password
     *
     * @param array $info
     * @return mixed user id or an error
     */
    public static function register(array $info)
    {
        self::$username = $info['username'];
        self::$email = $info['email'];

        $status = wp_create_user($info['username'],$info['password'],$info['email']);

        if( is_wp_error($status) )
        {
            return $status;
        }

        $user_id = $status;

        return $user_id;
    }

    /**
     * register user with the paired column name with keys
     *
     * @param array $info key value
     * @param array $columns
     */
    public static function customRegister(array $info,array $columns)
    {

    }

    /**
     * checks for existence of an user with email
     *
     * @param $email
     */
    public static function isExistByEmail($email)
    {

    }

    /**
     * checks for existence of an user with id
     *
     * @param $id
     */
    public static function isExistById($id)
    {

    }

    /**
     * checks for existence of an user with id
     *
     * @param $username
     */
    public static function isExistByUsername($username)
    {

    }

    /**
     * return boolean
     */
    public static function isBand()
    {

    }


    // http://codex.wordpress.org/Roles_and_Capabilities
    // https://managewp.com/create-custom-user-roles-wordpress

    /**
     * return role of the user with id
     *
     * @param $id
     * @return string $role
     */
    public static function role($id)
    {
//        get_role ()

        $role = "";

        return $role;
    }

    public static function addRole()
    {
//        add_role()
    }

    public static function removeRole()
    {
//        remove_role()
    }

    public static function addCapability()
    {
//        add_cap()
    }

    public static function removeCapability()
    {
//        remove_cap()
    }

    public static function hasCap()
    {
        current_user_can('moderate_comments');
    }
}