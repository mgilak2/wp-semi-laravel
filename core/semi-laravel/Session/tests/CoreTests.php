<?php

include __DIR__ . '/../core/Core.php';
use Wp\Session\Core\Core as Core;

class CoreTest extends PHPUnit_Framework_TestCase
{

    public function testGet()
    {
        $_SESSION['foo'] = 'bar';

        $this->assertEquals('bar',Core::get('foo'));
    }

    public function testPut()
    {
        Core::put('foo','bar');

        $this->assertEquals('bar',$_SESSION['foo']);
    }

    public function testHasTrue()
    {
        $_SESSION['foo'] = 'bar';

        $this->assertTrue(Core::has('foo'));
    }

    public function testHasFalse()
    {
        if(isset($_SESSION['foo']))
        {
            unset($_SESSION['foo']);
        }

        $this->assertFalse(Core::has('foo'));
    }

    public function testDelete()
    {
        $_SESSION['foo'] = 'bar';

        Core::delete( "foo" );

        $this->assertTrue( ! isset($_SESSION['foo']) );
    }

    /**
     * if Core::$started gets fill with true
     * it means that the session_start has been called with no error
     * as i noticed phpunit has something to do with session_start
     * and seems that during the execution this function was called once
     */
    public function testStart()
    {
        Core::start();

        $this->assertTrue( isset(Core::$started) );
    }
}