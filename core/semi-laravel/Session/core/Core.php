<?php namespace Wp\Session\Core;

class Core
{
    /**
     * @var bool $started
     */
    public static $started = false;

    /**
     *
     * first checks for existence of given name session
     * if exist then it returns it
     * if not returns null
     * it depends on self::has and self::get
     *
     * @param $name
     * @return mixed a session value
     */
    public static function get( $name )
    {        
        if( self::has($name) )
        {
            return $_SESSION[$name];
        }

        else
        {
            return null;
        }
    }

    /**
     * set a value to the session (key value style)
     *
     * @param $name
     * @param $value
     */
    public static function put( $name , $value )
    {
        self::start();

        $_SESSION[ $name ] = $value;
    }

    /**
     * checks whether your session is exist or not
     *
     * @param $name
     * @return bool
     */
    public static function has( $name )
    {        
        if( isset($_SESSION[$name]) )
        {
            return true;
        }
        
        else
        {
            return false;   
        }
    }

    /**
     * unset the named session
     *
     * @param $name
     */
    public static function delete( $name )
    {        
        if( self::has($name) )
        {
            unset( $_SESSION[$name] );   
        }
    }

    /**
     * starts the session (singleton style)
     * as its get call many times
     */
    public static function start()
    {
        if( self::$started === false )
        {
            @session_start();
            
            self::$started = true;
        }
    }
    
}