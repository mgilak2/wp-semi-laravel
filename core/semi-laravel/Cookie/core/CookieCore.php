<?php namespace Wp\Cookie\Core;

use App\Config\Config;

class CookieCore
{

    protected static $Encryptor = null;

    const CONFIG_NAME = "cookie";

    public static function get($key, $default="")
    {
        if(isset($_COOKIE[$key]))
        {
            return self::decrypt($_COOKIE[$key]);
        }
        else
        {
            return $default;
        }
    }

    public static function make($key, $value, $minute,$path = "/")
    {
        setcookie($key,self::encrypt($value), time()+60*$minute,$path);
    }

    public static function forever($key,$value)
    {
        setcookie($key,self::encrypt($value), time()+60*3600*24*60);
    }

    public static function forget($name)
    {
        setcookie ($name, "", time() - 3600);
    }

    public static function queue()
    {

    }

    public static function Encryptor()
    {
        if(self::$Encryptor === null)
        {
            self::$Encryptor =  RayCrypt::config(self::CONFIG_NAME, array('salt' => 'jdfjsddjcjef!$#*^dkasdjfsdfi7*@(@sdhf'));
        }

        return self::$Encryptor;
    }

    public static function encrypt($decrypted)
    {
        return RayCrypt::encrypt($decrypted, self::CONFIG_NAME);
    }

    public static function decrypt($encrypted)
    {
        return RayCrypt::decrypt($encrypted, self::CONFIG_NAME);
    }
}