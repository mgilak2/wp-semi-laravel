<?php namespace Wp\Ajax\Core;

class AjaxCore
{
    public static $ajaxes;

    public function add($name,$function)
    {
        self::$ajaxes[] =  [$name,$function];
    }

    public function init()
    {
        foreach(self::$ajaxes as $ajax)
        {
            $name = $ajax[0];
            $function = $ajax[1];

            if(is_array($function))
            {
                $name = $ajax[0];
                $class = $ajax[1][0];
                $function = $ajax[1][1];

                add_action('wp_ajax_'.$name, [$class,$function]);
                add_action('wp_ajax_nopriv_'.$name, [$class,$function]);

                return;
            }

            add_action('wp_ajax_'.$name,$function);
            add_action('wp_ajax_nopriv_'.$name,$function);
        }
    }


}