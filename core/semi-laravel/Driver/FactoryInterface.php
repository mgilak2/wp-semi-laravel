<?php namespace Wp\Driver;

interface FactoryInterface
{
    public function factory();
}