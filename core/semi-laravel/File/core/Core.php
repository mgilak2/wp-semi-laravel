<?php namespace Wp\File\Core;
use NishNosh\Admin\Repo\Crop as ACrop;

/**
 * Class Core
 * @package Wp\File\Core
 */
class Core
{
 
    public static function upload( $file )
    {
        if ( ! function_exists( 'wp_handle_upload' ) )
        {
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
        }

        $upload_overrides = array( 'test_form' => false );

        $movefile = wp_handle_upload( $file, $upload_overrides );

        if ( $movefile )
        {
            $data = new \stdClass();

            $data->path = $movefile['file'];

            $data->url = $movefile['url'];

            $data->type = $movefile['type'];

            return $data;
        }

        else
        {
            return false;
        }

    }
    
    public static function get()
    {
        
    }
    
    public static function remove()
    {
        
    }
    
    public static function is_exist()
    {
        
    }

    public static function downloadIt($baseAddress,$name,$url)
    {
        $fp = fopen($baseAddress . "/" . $name, 'w+'); // open file handle

        $ch = curl_init($url);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // enable if you want
        curl_setopt($ch, CURLOPT_FILE, $fp); // output to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000); // some large value to allow curl to run for a long time
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
        // curl_setopt($ch, CURLOPT_VERBOSE, true);   // Enable this line to see debug prints
        curl_exec($ch);

        curl_close($ch); // closing curl handle
        fclose($fp);
    }

    public static function get_file_name_with_its_url($url)
    {
        $tempArray = explode("/",$url);

        if($url[strlen($url) -1] == "/")
        {
            return $tempArray[count($tempArray) - 2];
        }

        else
        {
            return $tempArray[count($tempArray) - 1];
        }
    }

    public static function fetchFile($column,$input)
    {
        return [
            'name' => $_FILES[ $column ][ 'name' ][ $input ],
            'type' => $_FILES[ $column ][ 'type' ][ $input ],
            'tmp_name' => $_FILES[ $column ][ 'tmp_name' ][ $input ],
            'error' => $_FILES[ $column ][ 'error' ][ $input ],
            'size' => $_FILES[ $column ][ 'size' ][ $input ],
        ];
    }// end fetchFile


}