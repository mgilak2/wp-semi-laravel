<?php namespace Wp\Database\Core;

use Illuminate\Database\Capsule\Manager as Capsule;

class Core
{
    
    public static function table( $name , $func )
    {
        if( ! Capsule::schema()->hasTable( $name ) )
        {
            Capsule::schema()->create( $name , $func );
        }
                
    }
    
    public static function seed( $model , $args = array() , $func = null )
    {
        if( $func )
        {
            if(! $model::find(1))
            {
                $funcTemp['func'] =$func;

                $funcTemp['func']( $model );
            }
        }
        
        else
        {
            $model::create($args);
        }
        
    }

    public static function schema()
    {
        return Capsule::schema();
    }

    public static function prefix()
    {
        global $wpdb;
        return $wpdb->prefix;
    }
    
}