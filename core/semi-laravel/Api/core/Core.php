<?php namespace Wp\Api\Core;

class Core
{
    public static function deploy($address , $args)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,

            CURLOPT_URL => $address,

            CURLOPT_POST => 1,

            CURLOPT_POSTFIELDS => $args,

            CURLOPT_USERAGENT => 'Nish-o-Nosh'
        ));

        $resp = curl_exec($curl);

        curl_close($curl);

        return $resp;
    }
}