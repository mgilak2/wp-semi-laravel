<?php namespace Wp\Action\Core;

/**
* a wrapper for add_action in wordpress
*
* this package handle the add_action in a propper way
* for example some times you need your action to get call only 
* once or more times . i mean you can define how many time 
* execution you need . if you don`t notice it yet
* wordpress sometimes include functions.php many times
* and also executes its hooks many times too .
*
* @package Wp\Action
*/
class Core
{
    /**
    * @var integer $times it holds times of execution
    */
    public static $times = 1;
    
    /**
    * @var string $name it holds a name of hook
    */
    public static $name;
    
    /**
    * registers a hook to execute only one time 
    *
    * this function use anynomuse function for 
    * add_action second parameter .
    * what closure is use for is in add action
    * php storm throws an error if i just simply use
    * $func() . so i just did the trick in self::setClosure
    * it depends on Wp\Session package , add_action wordpress func
    * 
    * @see Wp\Session\Core\Core class 
    *
    * @param string $hook 
    * @param closure $func
    * @param string $name a name to store in session
    * @param integer $proyarity
    * @param integer $times 
    */
    public static function set( $hook , $func , $name , $proyarity = 10 , $times = 1 )
    {

        add_action( $hook , function() use($func , $name){

            if( ! \Session::has($name) )
            {
                $funcArray = array('func'=>$func);

                $funcArray['func']();

                \Session::put( $name , $name );
            }

            else
            {
                \Session::delete( $name );
            }

        }, $proyarity );
    }
    
    public static function checkHowManyTimes()
    {
        
        
    }
    
    /**
    * creates a custom hook
    *
    * this function use self::set function
    * it means your action get calls once 
    *
    * @param string $name a name for hook and store in session
    * @param function $func
    */
    public static function custom( $name , $func )
    {        
        self::set( $name , $func , $name );    
    }
    
    /**
    * calls a hook 
    *
    * it checks wheter this hook has been added or not
    * then if exist its going to call it with do_action
    * it depends on has_action , do_action wordpress func
    *
    * @param string $name your hook name
    */
    public static function call( $name )
    {
        if( has_action($name) )
        {
            do_action( $name );
        }
    }
    
    /**
    * unregisters an action
    *
    * this function checks for existance of your hook .
    * if it exist it removes an action
    * it depends on has_action , remove_action wordpress funcs
    *
    * @param string $name the same hook name you register before
    * @param function $func the same function you passed to register a hook with it
    * @param integer $proyarity the same proyaity you passed register a hook with it
    */
    public static function delete( $name , $func , $proyarity = 10 )
    {
        if( has_action( $name , $func , $proyarity ) )
        {
            remove_action( $name , $func , $proyarity );   
        }
    }
    
    /**
    * same thing to add_action
    *
    * @param string $name
    * @param function $func
    * @param integer $proyarity
    */
    public static function raw( $name , $func , $proyarity = 10 )
    {
        add_action( $name , $func , $proyarity );   
    }

    /**
     * sets the self::$times and some times self::$name
     *
     * this function could be use like Action::times(10,'somehook')
     * or like this Action::set()
     *
     * @param $times
     * @param null $name
     */
    public static function times( $times , $name = null )
    {
        if( $name )
        {
            self::$name = $name;    
        }
        
        self::$times = $times;
    }
    
    /**
    * checks for does it have to execute the action one more time ?
    *
    * this function use session and self::$times to wheter execute or not execute an action
    * in manner of wordpress execution flow 
    */
    public static function anotherTime()
    {
        
        
    }
    
    /**
    * 
    * some sort of test function to check times of executin 
    * of an action in manner of wordpress execution flow
    *
    * @param string $name name of a action you did set before
    */
    public static function howManyTime( $name )
    {
        
        
    }
}
