<?php namespace Wp\Validation\Core;

use \Wp\Validation\Interfaces\LanguageInterface,
    \Wp\Driver\FactoryInterface;

class ValidatorCore implements FactoryInterface,LanguageInterface
{
    /**
     * @see Wp\Validation\Core\ValidatorCore::resolveGump
     * @var \Gump
     */
    public static $gump = null;

    /**
     * @see Wp\Validation\Core\ValidatorCore::fails
     * @var bool
     */
    public static $fails = false;

    /**
     * @see Wp\Validation\Core\ValidatorCore::passes
     * @var bool
     */
    public static $passes = true;

    /**
     * @var array
     */
    public static $hooks = array();

    public static $messages ;

    /**
     * @param array $inputs key value
     * @param array $rules key value
     * @param array $messages key value
     * @return mixed
     */
    public static function make(array $inputs = array(),array $rules = array(),array $messages = array())
    {
        $gump = self::resolveGump();

        $inputs = $gump->sanitize($inputs);

        if(! empty($rules))
        {
            $gump->validation_rules($rules);
        }

        if(! empty($messages))
        {
            $gump->customMessages = $messages;
        }

        $result = $gump->run($inputs);
        $validator = new self;
        if($result === false)
        {
            $errors = $gump->get_readable_errors(true);

            $validator->fails = true;

            $validator->messages = $gump->objectiveError;

            return $validator;
        }

        $validator->fails = true;

        return $validator;
    }

    public static function fails()
    {
        if(self::$fails == true)
        {
            return true;
        }

        return false;
    }

    public static function passes()
    {

    }

    public static function messages()
    {
        return self::$messages;
    }

    public static function hook($key,$func)
    {

    }

    /**
     * singleton method
     * @return \GUMP
     */
    public static function resolveGump()
    {
        if(self::$gump === null)
        {
            self::$gump = new GumpExtends();
        }

        return self::$gump;
    }

    public static function resolveMessages()
    {

    }

    public function factory()
    {
        return new ValidatorCore();
    }

    public function getLanguage()
    {
        // TODO: Implement getLanguage() method.
    }

    public function getDefaultMessages()
    {
        // TODO: Implement getDefaultMessages() method.
    }
}