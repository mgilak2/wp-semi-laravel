<?php namespace Wp\Validation\Core;

class Messages
{
    protected $errors;

    protected $fieldName = "";

    protected $namespacedMessages = array();

    public function __construct($fieldName,$namespacedMessages)
    {
        $this->fieldName = $fieldName;
        $this->namespacedMessages = $namespacedMessages;
    }

    public $all = array();

    public function first()
    {
        if(count($this->all))
        {
            return $this->all[0];
        }
    }

    public function all()
    {
        return $this->all;
    }

    public function get($key)
    {

        if(isset($this->namespacedMessages[$this->fieldName][$key]))
        {
            return $this->namespacedMessages[$this->fieldName][$key];
        }

        if(isset($this->all[$key]))
        {
            return $this->all[$key];
        }
    }

}