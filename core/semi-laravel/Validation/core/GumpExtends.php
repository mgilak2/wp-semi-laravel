<?php namespace Wp\Validation\Core;

class GumpExtends extends \Gump
{
    public $processed = false;

    public $namespacedMessage = array();

    public $messages = [
        'mismatch'              =>"There is no validation rule for :field",
        'validate_required'     =>"The :field field is required",
        'validate_valid_email'  =>"The :field field is required to be a valid email address",
        'validate_max_len'      => "The :field field needs to be shorter than :param character",
        'validate_min_len'      => "The :field field needs to be longer than :param length",
        'validate_exact_len'    => "The :field field needs to be exactly :param characters in length",
        'validate_alpha'        => "The :field field may only contain alpha characters(a-z)",
        'validate_alpha_numeric'=> "The :field field may only contain alpha-numeric characters",
        'validate_alpha_dash'   => "The :field field may only contain alpha characters &amp; dashes",
        'validate_numeric'      => "The :field field may only contain numeric characters",
        'validate_integer'      => "The :field field may only contain a numeric value",
        'validate_boolean'      => "The :field field may only contain a true or false value",
        'validate_float'        => "The :field field may only contain a float value",
        'validate_valid_url'    => "The :field field is required to be a valid URL",
        'validate_url_exists'   => "The :field URL does not exist",
        'validate_valid_ip'     => "The :field field needs to contain a valid IP address",
        'validate_valid_cc'     => "The :field field needs to contain a valid credit card number",
        'validate_valid_name'   => "The :field field needs to contain a valid human name",
        'validate_contains'     => "The :field field needs to contain one of these values: :param",
        'validate_street_address'=>"The :field field needs to be a valid street address",
        'validate_date'        => "The :field field needs to be a valid date",
        'validate_min_numeric' => "The :field field needs to be a numeric value, equal to, or higher than :param",
        'validate_max_numeric' => "The :field field needs to be a numeric value, equal to, or lower than :param",
    ];

    /**
     * @param string $name
     * @param string $field
     * @param string $param
     * @return mixed
     */
    public function rules($name = "",$field = "",$param = "",$realName="")
    {
        if(isset($this->customMessages) && ! $this->processed)
        {
            $this->processCustomMessage($this->customMessages);
        }

        $message = $this->replace($field,$param,$this->messages[$name]);



        $this->objectiveError($realName,$name,$message);

        return $message;
    }

    /**
     * @param $field
     * @param $name
     * @param $message
     */
    public function objectiveError($field,$name,$message)
    {
        $validationName = str_replace("validate_","",$name);

        $this->resolveObjectiveError();
        $this->resolveFieldObjectiveError($field);

        $this->objectiveError->$field->$validationName = $message;
        $this->objectiveError->$field->all[$validationName] = $message;
    }

    public function resolveObjectiveError()
    {
        if(! isset($this->objectiveError))
        {
            $this->objectiveError = new \stdClass();
        }
    }

    public function resolveFieldObjectiveError($field)
    {
        if(! isset($this->objectiveError->$field))
        {
            $this->objectiveError->$field = new Messages($field,$this->namespacedMessage);
            $this->objectiveError->$field->all = array();
        }
    }

    /**
     * @param array $customMessage
     */
    public function processCustomMessage($customMessage)
    {
        $this->processed = true;

        foreach ($customMessage as $field => $message)
        {
            if(strrpos($field,".") === false)
            {
                $this->messages["validate_".$field] = $message;
            }

            elseif(strrpos($field,".") !== false)
            {
                $this->messages["validate_".$field] = $message;

                $fieldAndValidation = explode('.',$field);

                $this->namespacedMessage[$fieldAndValidation[0]][$fieldAndValidation[1]] = $message;
            }
        }

    }

    /**
     * @param string $field
     * @param string $param
     * @param $message
     * @return string
     */
    public function replace($field = "",$param="",$message)
    {
        if( strrpos($message,":field") !== false && strrpos($message,":param") === false )
        {
            return $this->replaceField($field,$message);
        }

        if( strrpos($message,":field") !== false && strrpos($message,":param") !== false && ! is_array($param) )
        {
            return $this->replaceFieldAndParam($field,$param,$message);
        }

       if( strrpos($message,":field") !== false && strrpos($message,":param") && is_array($param) )
       {
            return $this->replaceFieldAndArrayParam($field,$param,$message);
       }

        return $message;
    }

    /**
     * @param string $field
     * @param string $message
     * @return string
     */
    public function replaceField($field,$message)
    {
        return str_replace(":field",$field,$message);
    }

    /**
     * @param string $field
     * @param string $param
     * @param string $message
     * @return string
     */
    public function replaceFieldAndParam($field,$param,$message)
    {
        $message = str_replace(":field",$field,$message);
        return str_replace(":param",$param,$message);
    }

    /**
     * @param string $field
     * @param array $param
     * @param string $message
     * @return string
     */
    public function replaceFieldAndArrayParam($field,$param,$message)
    {
        $message = str_replace(":field",$field,$message);
        return str_replace(":param",implode(',',$param),$message);
    }

    /**
     * Process the validation errors and return human readable error messages
     *
     * @param bool $convert_to_string = false
     * @param string $field_class
     * @param string $error_class
     * @return array
     * @return string
     */
    public function get_readable_errors($convert_to_string = false, $field_class="field", $error_class="error-message")
    {
        if(empty($this->errors)) {
            return ($convert_to_string)? null : array();
        }

        $resp = array();

        foreach($this->errors as $e) {

            $field = ucwords(str_replace(array('_','-'), chr(32), $e['field']));
            $param = $e['param'];

            switch($e['rule']) {
                case 'mismatch' :
                    $resp[] = $this->rules("mismatch",$field,$param,$e['field']);
                    break;
                case 'validate_required':
                    $resp[] = $this->rules("validate_required",$field,$param,$e['field']);
                    break;
                case 'validate_valid_email':
                    $resp[] = $this->rules("validate_valid_email",$field,$param,$e['field']);
                    break;
                case 'validate_max_len':
                    if($param == 1) {
                        $resp[] = $this->rules("validate_max_len",$field,$param,$e['field']);
                    } else {
                        $resp[] = $this->rules("validate_max_len",$field,$param,$e['field']);
                    }
                    break;
                case 'validate_min_len':
                    if($param == 1) {
                        $resp[] = $this->rules("validate_min_len",$field,$param,$e['field']);
                    } else {
                        $resp[] = $this->rules("validate_min_len",$field,$param,$e['field']);
                    }
                    break;
                case 'validate_exact_len':
                    if($param == 1) {
                        $resp[] = $this->rules("validate_exact_len",$field,$param,$e['field']);
                    } else {
                        $resp[] = $this->rules("validate_exact_len",$field,$param,$e['field']);
                    }
                    break;
                case 'validate_alpha':
                    $resp[] = $this->rules("validate_alpha",$field,$param,$e['field']);
                    break;
                case 'validate_alpha_numeric':
                    $resp[] = $this->rules("validate_alpha_numeric",$field,$param,$e['field']);
                    break;
                case 'validate_alpha_dash':
                    $resp[] = $this->rules("validate_alpha_dash",$field,$param,$e['field']);
                    break;
                case 'validate_numeric':
                    $resp[] = $this->rules("validate_numeric",$field,$param,$e['field']);
                    break;
                case 'validate_integer':
                    $resp[] = $this->rules("validate_integer",$field,$param,$e['field']);
                    break;
                case 'validate_boolean':
                    $resp[] = $this->rules("validate_boolean",$field,$param,$e['field']);
                    break;
                case 'validate_float':
                    $resp[] = $this->rules("validate_float",$field,$param,$e['field']);
                    break;
                case 'validate_valid_url':
                    $resp[] = $this->rules("validate_valid_url",$field,$param,$e['field']);
                    break;
                case 'validate_url_exists':
                    $resp[] = $this->rules("validate_url_exists",$field,$param,$e['field']);
                    break;
                case 'validate_valid_ip':
                    $resp[] = $this->rules("validate_valid_ip",$field,$param,$e['field']);
                    break;
                case 'validate_valid_cc':
                    $resp[] = $this->rules("validate_valid_cc",$field,$param,$e['field']);
                    break;
                case 'validate_valid_name':
                    $resp[] = $this->rules("validate_valid_name",$field,$param,$e['field']);
                    break;
                case 'validate_contains':
                    $resp[] = $this->rules("validate_contains",$field,$param,$e['field']);
                    break;
                case 'validate_street_address':
                    $resp[] = $this->rules("validate_street_address",$field,$param,$e['field']);
                    break;
                case 'validate_date':
                    $resp[] = $this->rules("validate_date",$field,$param,$e['field']);
                    break;
                case 'validate_min_numeric':
                    $resp[] = $this->rules("validate_min_numeric",$field,$param,$e['field']);
                    break;
                case 'validate_max_numeric':
                    $resp[] = $this->rules("validate_max_numeric",$field,$param,$e['field']);
                    break;
                default:
                    $resp[] = "The <span class=\"$field_class\">$field</span> field is invalid";
            }
        }

        if(!$convert_to_string) {
            return $resp;
        }

        else {
            $buffer = '';
            foreach($resp as $s) {
                $buffer .= $s;
            }
            return $buffer;
        }
    }

}

