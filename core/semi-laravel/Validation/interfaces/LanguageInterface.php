<?php namespace Wp\Validation\Interfaces;

interface LanguageInterface
{
    public function getLanguage();

    public function getDefaultMessages();

}