<?php namespace Wp\Validation\Core;

class ValidationDriver extends \Wp\Driver\Driver
{
    public $dependencies = [
        "Language"=>'\\Wp\\Language\\Language',
    ];

    public $native = [
        'Language'=>"\\Wp\\Validation\\Core\\Multilingual",
    ];
}