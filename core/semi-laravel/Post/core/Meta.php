<?php namespace Wp\Post\Core;

class Meta extends \Eloquent
{
    protected $table = 'wp_postmeta';

    protected $primaryKey = "meta_id";

    public $timestamps = false;

    public function metas()
    {
        return $this->belongsTo('\Wp\Post\Post');
    }
}