<?php namespace Wp\Post\Core;

class Model extends \Eloquent
{
    protected $table = 'wp_posts';

    protected $primaryKey = "ID";

    public $timestamps = false;

}
