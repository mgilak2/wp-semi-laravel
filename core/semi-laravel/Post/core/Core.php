<?php namespace Wp\Post\Core;

class Core extends \Wp\Post\Core\Model
{
    public static function afterPublished($name,$func)
    {
        \Action::raw('transition_post_status',function($new_status, $old_status, $post) use($func,$name)
        {
            if($post->post_status == 'publish')
            {
                if( ! \Session::has($name) )
                {
                    $funcArray = array('func'=>$func);

                    $publishedPost = Post::where('post_id','=',$post->ID)->first();

                    if(isset($publishedPost->id))
                    {
                        $publishedPost->post_title = $post->post_title;

                        $publishedPost->save();
                    }

                    else
                    {
                        $publishedPost = new Post;

                        $publishedPost->post_title = $post->post_title;

                        $publishedPost->post_id = $post->ID;

                        $publishedPost->save();
                    }

                    \Session::put( $name , $name );
                }

                else
                {
                    \Session::delete( $name );
                }
        }
        },10 , 3);
    }

}