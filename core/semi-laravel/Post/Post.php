<?php namespace Wp\Post;

class Post extends \Wp\Post\Core\Core
{
    public function metas()
    {
        return $this->hasMany('\Wp\Post\Core\Meta');
    }
}