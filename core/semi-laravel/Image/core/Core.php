<?php namespace Wp\Image\Core;

class Core
{
    public static function thumbnail()
    {

    }

    public static function mimeType($name)
    {
        $temp = explode(".",$name);

        $mimeType = $temp[count($temp)-1];

        if($mimeType == 'png')
        {
            return 'png';
        }

        elseif($mimeType == 'jpeg')
        {
            return 'jpeg';
        }

        elseif($mimeType == 'jpg')
        {
            return 'jpg';
        }
    }

    /**
     * @param $img
     * $img->name , $img->mimeType
     * $img->savePath
     * $img->imgRes,$img->quality
     */
    public static function uploadImage($img)
    {
        if( 'png' == $img->mimeType)
        {
            imagepng($img->imgRes,$img->savePath,$img->quality);
        }

        elseif('jpeg' == $img->mimeType)
        {
            imagejpeg($img->imgRes,$img->savePath,$img->quality);
        }
    }

    /**
     * @param string $format
     * @param $address
     * @return resource
     */
    public static function createImage($format = 'png',$address)
    {
        if($format == 'png')
        {
            return imagecreatefrompng($address);
        }

        elseif($format == 'jpeg' || $format == 'jpg')
        {
            return imagecreatefromjpeg($address);
        }
    }

    public static function saveImage($img)
    {
        if( 'png' == $img->mimeType)
        {
            return imagepng($img->imgRes,$img->savePath,$img->quality);
        }

        elseif('jpeg'== $img->mimeType || 'jpg'== $img->mimeType)
        {
            return imagejpeg($img->imgRes,$img->savePath,$img->quality);
        }
    }
}