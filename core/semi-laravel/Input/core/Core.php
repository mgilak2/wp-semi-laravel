<?php namespace Wp\Input\Core;

class Core
{
    /**
     * give back the existed get , put and other methods
     * which have global variables with the help oh $_REQUEST
     * if nothing found returns null
     *
     * @param $param
     * @return array|null
     */
    public static function get( $param )
    {
        if( is_string( $param ) )
        { 
            if( isset($_REQUEST[$param]) )
            {
                return $_REQUEST[$param];
            }

            else
            {
                return false;
            }
        }
        
        elseif( is_array( $param ) )
        {
            $params = array();
            
            foreach($param as $key )
            {
                $params[ $key ] = $_REQUEST[ $key ] ;
            }
            
            return $params;
        }

        else
        {
            return null;
        }
    }

    /**
     * returns all sended data back
     * @return mixed
     */
    public static function all()
    {
        return $_REQUEST;
    }

    /**
     * return true if named parameter founded
     * in $_REQUEST
     *
     * must add : also take array as parameter
     *
     * @param $param
     * @return bool
     */
    public static function has( $param )
    {
        if( isset($_REQUEST[$param]) )
        {
            return true;
        }
        
        else
        {
            return false;   
        }
    }

    /**
    * unset the existed item in Request
    *
    * @param string $param
    */
    public static function delete($param)
    {
        if( isset($_REQUEST[$param]) )
        {
            unset($_REQUEST[$param]);
        }
    }

    /**
     * havent do anything with till now
     *
     * @param string $name
     * @param null $inputs
     */
    public static function remember( $name = 'inputs' ,$inputs = null )
    {
        if($inputs)
        {
            \Session::put( $name , self::get($inputs) );
        }
        else
        {
            \Session::put( $name , self::all() );
        }
    }
    
}