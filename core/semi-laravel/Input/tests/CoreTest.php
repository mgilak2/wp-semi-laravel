<?php

include '../core/Core.php';
use Wp\Input\Core\Core as Core;

class CoreTest extends PHPUnit_Framework_TestCase
{

    public function testGetString()
    {
        $_REQUEST['foo'] = 'bar';

        $this->assertEquals('bar', Core::get('foo'));
    }

    public function testGetArray()
    {
        $_REQUEST['foo'] = 'baz';
        $_REQUEST['bar'] = 'o';

        $this->assertEquals(['foo'=>'baz','bar'=>'o'],Core::get(['foo','bar']));
    }

    public function testGetNull()
    {
        $this->assertNull(Core::get(null));
    }

    public function testAll()
    {
        $data = ['foo'=>'bar','baz'=>'o'];

        $_REQUEST['foo'] = 'bar';
        $_REQUEST['baz'] = 'o';

        $this->assertEquals($data,Core::all());
    }

    public function testHas()
    {
        $_REQUEST['foo'] = 'bar';

        $this->assertTrue(Core::has('foo'));
    }

}