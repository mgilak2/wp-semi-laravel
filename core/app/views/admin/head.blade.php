<!-- main JS libs -->
<script src="{{ asset('js/libs/modernizr.min.js') }}"></script>
<script src="{{ asset('js/libs/jquery-1.10.2.min.js') }}"></script>
<script src="{{ asset('js/libs/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/libs/bootstrap.min.js') }}"></script>

<!-- Style CSS -->
<link href="{{ asset('css/bootstrap.css') }}" media="screen" rel="stylesheet">
<link href="{{ asset('css/admin.css') }}" media="screen" rel="stylesheet">

<style>

</style>

<!-- General Scripts -->
<script src="{{ asset('js/general.js') }}"></script>

<!-- custom inpu -->
<script src="{{ asset('js/jquery.customInput.js') }}"></script> 

<!-- Placeholders -->
<script type="text/javascript" src="{{ asset('js/jquery.powerful-placeholder.min.js') }}"></script>
<script>
    jQuery(document).ready(function($) {
        if($("[placeholder]").size() > 0) {
            $.Placeholder.init();
        }
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#contact-name').chosen({ width: "100%" });
        jQuery('#commentForm .link-reset').click(function(){
            jQuery("#contact-name").trigger("chosen:updated");
        });
    });
</script>

<!-- Lightbox prettyPhoto -->
<link href="{{ asset('css/prettyPhoto.css') }}" rel="stylesheet">
<script src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>
<!-- Visual Text Editor (NicEdit.js) -->
<script src="{{ asset('js/nicEdit.js') }}"></script>

<!-- Multiselect (Chosen.js) -->
<link rel="stylesheet" href="{{ asset('css/chosen.css') }}">
<script src="{{ asset('js/chosen.jquery.min.js') }}" type="text/javascript"></script>


<!--[if lt IE 9]><script src="js/respond.min.js"></script><![endif]-->
<!--[if gte IE 9]>
<style type="text/css">
    .gradient {filter: none !important;}
</style>
<![endif]-->

<link rel="stylesheet" href="{{ asset('core/app/views/admin/style.css') }}">
<script src="{{ asset('core/app/views/admin/script.js') }}" type="text/javascript"></script>