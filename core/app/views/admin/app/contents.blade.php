<style>
.appEditor .nicEdit-panel 
{
    overflow: hidden;
    position: absolute;
    left: 0;
    top: 55px;
}
</style>

<div class="title">
    متن معرفی اپلیکیشن
</div>

<div class="appEditor">
    
    <div class="field_text lightPlaceholder" style="float: right;direction: rtl;z-index: 1000;width:100% !important">
        <label for="app-subject" class="label_title">عنوان:</label>
        {{ Form::text(['name'=>'app[subject]','id'=>'app-subject','style'=>'float:right']) }}
    </div>

    <div class="field_text field_textarea" style="margin-bottom:15px !important">
        {{ Form::long(['name'=>"app[content]",'id'=>"app-content"]) }}
    </div>
    
</div>
           