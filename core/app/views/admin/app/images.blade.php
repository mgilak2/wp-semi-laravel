<style>

    .field_text
    {
        margin-right: 0;
    }
    
</style>   

<div class="title">
    عکس ها
</div>

<div class="app-buttons">

    <!-- Iphone -->
    <div class="field_text lightPlaceholder intro-upload" >
        <label for="iphone" class="label_title">آیفون:</label>
        {{ Form::file(['name'=>'app[iphone]','id'=>'app-iphone','class'=>'file-input']) }}
    </div>

    <!-- Network -->
    <div class="field_text lightPlaceholder intro-upload">
        <label for="tablet" class="label_title">تبلت:</label>
        {{ Form::file(['name'=>'app[tablet]','id'=>'tablet','class'=>'file-input']) }}
    </div>

    <!-- Background -->
    <div class="field_text lightPlaceholder intro-upload">
        <label for="app-background" class="label_title">پس زمینه:</label>
        {{ Form::file(['name'=>'app[background]','id'=>'app-background','class'=>'file-input']) }}
    </div>

</div>