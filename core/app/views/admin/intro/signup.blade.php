<div class="title">
    عضویت
</div>

<div class="intro-signup-wraper">

    <div class="field_text lightPlaceholder">
        <label for="intro-signup-subject" class="label_title">عنوان:</label>
        {{ Form::text(['name'=>'intro[signupSubject]','id'=>'intro-signup-subject']) }}
    </div>

    <div class="field_text field_textarea" style="margin-bottom:15px !important">
        {{ Form::long(['name'=>'intro[signupContent]','id'=>'intro-signup-content']) }}
    </div>

    <div class="field_text lightPlaceholder" style="margin-bottom:0 !important">
        <label for="intro-signup-button" class="label_title">دکمه:</label>
        {{ Form::text(['name'=>'intro[signupButton]','id'=>'intro-signup-button']) }}
    </div>
    
</div>
