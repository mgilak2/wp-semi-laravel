<!-- //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////// -->                                               
 <div class="col-sm-12">
    <!-- Contact form -->
    <div class="add-comment styled boxed" id="addcomments">
        <div class="add-comment-title"><h3>چیزی بنویسید</h3></div>
        <div class="comment-form">
            <!-- Fire Text Editor -->
            <div action="#" method="post" id="commentForm">
                <div class="form-inner">
             
                    @include('admin.intro.contents')
                    
                    <div style="margin: 30px 0 30px;" class="flatline"></div>
                        
                    @include('admin.intro.images')                
                   
                    <div style="margin: 30px 0 30px;" class="flatline"></div>
                    
                    @include('admin.intro.buttons')
                    
                    <div style="margin: 30px 0 30px;" class="flatline"></div>
                    
                    @include('admin.intro.signup')
                    
                </div>
               
            </div>
        </div>
    </div>
<!--/ Contact form -->
</div>



