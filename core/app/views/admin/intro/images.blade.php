<div class="title">
    عکس ها
</div>

<!-- Iphone -->
<div class="field_text lightPlaceholder intro-upload" >
    <label for="intro-iphone" class="label_title">آیفون:</label>
    {{ Form::file(['name'=>'intro[iphone]','id'=>'intro-iphone','class'=>'file-input']) }}
</div>

<!-- Network -->
<div class="field_text lightPlaceholder intro-upload">
    <label for="intro-network" class="label_title">شبکه:</label>
    {{ Form::file(['name'=>'intro[network]','id'=>'intro-network','class'=>'file-input']) }}
</div>

<!-- Background -->
<div class="field_text lightPlaceholder intro-upload">
    <label for="intro-background" class="label_title">پس زمینه:</label>
    {{ Form::file(['class'=>'file-input','name'=>'intro[background]','id'=>'intro-background']) }}
</div>
