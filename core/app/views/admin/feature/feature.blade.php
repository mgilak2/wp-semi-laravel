<div class="title">
    ویژگی ها
</div>

@include('admin.feature.title')

<div class="col-sm-6">
    <!-- Contact form -->
    <div class="add-comment styled boxed" id="addcomments">
        <div class="add-comment-title"><h3>ویژگی ۱</h3></div>
        <div class="comment-form">

            <div id="commentForm">
                <div class="form-inner">

                    <div class="field_text lightPlaceholder">
                        <label for="subject" class="label_title">عنوان:</label> 
                        {{ Form::text(['name'=>'feature[subject1]','id'=>'feature-subject-1']) }}
                    </div>
                    <div class="field_text field_textarea">
                        {{ Form::long(['name'=>'feature[content1]','id'=>'feature-1']) }}
                    </div>
                    
                    <div class="field_text lightPlaceholder">
                        <label for="icon-1" class="label_title">عنوان:</label>
                        {{ Form::file(['class'=>'file-input','name'=>'feature[icon1]','id'=>'icon-1']) }}
                    </div>
                </div>
                
            </div>
        </div>
    </div>
<!--/ Contact form -->
</div>

<!-- //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////// -->                                               
 <div class="col-sm-6">
    <!-- Contact form -->
    <div class="add-comment styled boxed" id="addcomments">
        <div class="add-comment-title"><h3>ویژگی ۲</h3></div>
        <div class="comment-form">
            <!-- Fire Text Editor -->
            <script type="text/javascript">
                
            </script>
            <div id="commentForm">
                <div class="form-inner">

                    <div class="field_text lightPlaceholder">
                        <label for="feature-subject-2" class="label_title">عنوان:</label>
                        {{ Form::text(['name'=>'feature[subject2]','id'=>'feature-subject-2']) }}
                    </div>

                    <div class="field_text field_textarea">
                        {{ Form::long(['name'=>'feature[content2]','id'=>'feature-2']) }}
                    </div>
                    
                    <div class="field_text lightPlaceholder">
                        <label for="feature-icon-2" class="label_title">عنوان:</label>
                        {{ Form::file(['name'=>'feature[icon2]','id'=>'feature-icon-2','class'=>'file-input']) }}
                    </div>
                </div>
                
            </div>
        </div>
    </div>
<!--/ Contact form -->
</div>

<!-- //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////// -->                                               
 <div class="col-sm-6">
    <!-- Contact form -->
    <div class="add-comment styled boxed" id="addcomments">
        <div class="add-comment-title"><h3>ویژگی ۳</h3></div>
        <div class="comment-form">
            <!-- Fire Text Editor -->
            <script type="text/javascript">
                
            </script>
            <div id="commentForm">
                <div class="form-inner">

                    <div class="field_text lightPlaceholder">
                        <label for="feature-subject-3" class="label_title">عنوان:</label>
                        {{ Form::text(['name'=>'feature[subject3]']) }}
                    </div>
                    <div class="field_text field_textarea">
                        {{ Form::long(['name'=>'feature[content3]','id'=>'feature-3']) }}
                    </div>
                    
                    <div class="field_text lightPlaceholder">
                        <label for="feature-icon-3" class="label_title">عنوان:</label>
                        {{ Form::file(['class'=>'file-input','name'=>'feature[icon3]','id'=>'feature-icon-3']) }}
                    </div>
                </div>
                
            </div>
        </div>
    </div>
<!--/ Contact form -->
</div>


<div class="col-sm-6">
    <!-- Contact form -->
    <div class="add-comment styled boxed" id="addcomments">
        <div class="add-comment-title"><h3>ویژگی ۴</h3></div>
        <div class="comment-form">
            <div id="commentForm">
                <div class="form-inner">

                    <div class="field_text lightPlaceholder">
                        <label for="feature-subject-4" class="label_title">عنوان:</label>
                        {{ Form::text(['name'=>'feature[subject4]','id'=>'feature-subject-4']) }}
                    </div>
                    <div class="field_text field_textarea">
                        {{ Form::long(['name'=>'feature[content4]','id'=>'feature-4']) }}
                    </div>
                    
                    <div class="field_text lightPlaceholder">
                        <label for="icon2" class="label_title">عنوان:</label>
                        {{ Form::file(['name'=>'feature[icon4]','id'=>'feature-icon-4','class'=>'file-input']) }}
                    </div>
                </div>
                
            </div>
        </div>
    </div>
<!--/ Contact form -->
</div>

<!-- //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////// -->                                               
 <div class="col-sm-6">
    <!-- Contact form -->
    <div class="add-comment styled boxed" id="addcomments">
        <div class="add-comment-title"><h3>ویژگی ۵</h3></div>
        <div class="comment-form">
            <!-- Fire Text Editor -->
            <script type="text/javascript">
                
            </script>
            <div id="commentForm">
                <div class="form-inner">
                    <div class="field_text lightPlaceholder">
                        <label for="feature-subject-5" class="label_title">عنوان:</label>
                        {{ Form::text(['name'=>'feature[subject5]','id'=>'feature-subject-5']) }}
                    </div>

                    <div class="field_text field_textarea">
                        {{ Form::long(['name'=>'feature[content5]','id'=>'feature-5']) }}
                    </div>
                    
                    <div class="field_text lightPlaceholder">
                        <label for="icon2" class="label_title">عنوان:</label>
                        {{ Form::file(['name'=>'feature[icon5]','class'=>'file-input','id'=>'feature-icon-5']) }}
                    </div>
                </div>
                
            </div>
        </div>
    </div>
<!--/ Contact form -->
</div>



<div class="col-sm-6">
    <!-- Contact form -->
    <div class="add-comment styled boxed" id="addcomments">
        <div class="add-comment-title"><h3>ویژگی ۶</h3></div>
        <div class="comment-form">

            <div id="commentForm">
                <div class="form-inner">

                    <div class="field_text lightPlaceholder">
                        <label for="feature-subject-6" class="label_title">عنوان:</label>
                        {{ Form::text(['name'=>'feature[subject6]','id'=>'feature-subject-6']) }}
                    </div>
                    <div class="field_text field_textarea">
                        {{ Form::long(['name'=>'feature[content6]','id'=>'feature-6']) }}
                    </div>
                    
                    <div class="field_text lightPlaceholder">
                        <label for="icon2" class="label_title">عنوان:</label>
                        {{ Form::file(['name'=>'feature[icon6]','class'=>'file-input','id'=>'feature-icon-6']) }}
                    </div>
                </div>
                
            </div>
        </div>
    </div>
<!--/ Contact form -->
</div>

<!-- //////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////// -->                                   

      
<div class="col-sm-6">
    <!-- Contact form -->
    <div class="add-comment styled boxed" id="addcomments">
        <div class="add-comment-title"><h3>ویژگی ۷</h3></div>
        <div class="comment-form">

            <div id="commentForm">
                <div class="form-inner">

                    <div class="field_text lightPlaceholder">
                        <label for="feature-subject-7" class="label_title">عنوان:</label>
                        {{ Form::text(['name'=>'feature[subject7]','id'=>'feature-subject-7']) }}
                    </div>
                    <div class="field_text field_textarea">
                        {{ Form::long(['name'=>'feature[content7]','id'=>'feature-7']) }}
                    </div>
                    
                    <div class="field_text lightPlaceholder">
                        <label for="feature-icon-7" class="label_title">عنوان:</label>
                        {{ Form::file(['class'=>'file-input','name'=>'feature[icon7]','id'=>'feature-icon-7']) }}
                    </div>
                </div>
                
            </div>
        </div>
    </div>
<!--/ Contact form -->
</div>
                  
 <div class="col-sm-6">
    <!-- Contact form -->
    <div class="add-comment styled boxed" id="addcomments">
        <div class="add-comment-title"><h3>ویژگی ۸</h3></div>
        <div class="comment-form">

            <div id="commentForm">
                <div class="form-inner">

                    <div class="field_text lightPlaceholder">
                        <label for="feature-subject-8" class="label_title">عنوان:</label>
                        {{ Form::text(['name'=>'feature[subject8]','id'=>'feature-subject-8']) }}
                    </div>
                    <div class="field_text field_textarea">
                        {{ Form::long(['name'=>'feature[content8]','id'=>'feature-8']) }}
                    </div>
                    
                    <div class="field_text lightPlaceholder">
                        <label for="icon2" class="label_title">عنوان:</label>
                        {{ Form::file(['name'=>'feature[icon8]','id'=>'feature-icon-8','class'=>'file-input']) }}
                    </div>
                </div>
                
            </div>
        </div>
    </div>
<!--/ Contact form -->
</div>
