<div class="title">
    متن تماس با ما
</div>
<div class="appEditor">
    
    <div class="field_text lightPlaceholder" style="float: right;z-index: 1000;width: 100%">
        <label for="contact-subject" class="label_title">عنوان:</label>
        {{ Form::text(['name'=>'contact[subject]','id'=>'contact-subject']) }}
    </div>

    <div class="field_text field_textarea" style="margin-bottom:15px !important">
        {{ Form::long(['name'=>'contact[content]','id'=>'contact-content']) }}
    </div>
    
    <div class="field_text lightPlaceholder" style="margin-bottom:0 !important;float: right">
    <label for="subject" class="label_title">دکمه:</label>
        {{ Form::text(['name'=>'contact[button]','id'=>'contact-button']) }}
</div>
    
</div>
           