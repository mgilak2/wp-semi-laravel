// admin / app / contents
bkLib.onDomLoaded(function() {
var myNicEditor = new nicEditor({
    buttonList : [
        'bold',
        'italic',
        'underline',
        'forecolor',
        'left',
        'center',
        'right',
        'justify'
    ]
});
myNicEditor.panelInstance('app-content');

jQuery('#commentForm .link-reset').click(function(){
    myNicEditor.removeInstance('app-content');
    $('#app-content').val('');
    myNicEditor.panelInstance('app-content');
});
});

//
//////////////////////////////////////////////////////
//
//
//// admin / contact / contact

bkLib.onDomLoaded(function() {
var myNicEditor = new nicEditor({
    buttonList : [
        'bold',
        'italic',
        'underline',
        'forecolor',
        'left',
        'center',
        'right',
        'justify'
    ]
});
myNicEditor.panelInstance('contact-content');

jQuery('#commentForm .link-reset').click(function(){
    myNicEditor.removeInstance('contact-content');
    $('#contact-content').val('');
    myNicEditor.panelInstance('contact-content');
});
});

////////////////////////////////////////////////////////
//
//// admin / feature / feature


////////// feature 1
bkLib.onDomLoaded(function() {
var myNicEditor = new nicEditor({
    buttonList : [
        'bold',
        'italic',
        'underline',
        'forecolor',
        'left',
        'center',
        'right',
        'justify'
    ]
});
myNicEditor.panelInstance('feature-1');

jQuery('#commentForm .link-reset').click(function(){
    myNicEditor.removeInstance('feature-1');
    $('#feature-1').val('');
    myNicEditor.panelInstance('feature-1');
});
});


///// feature-2
bkLib.onDomLoaded(function() {
var myNicEditor = new nicEditor({
    buttonList : [
        'bold',
        'italic',
        'underline',
        'forecolor',
        'left',
        'center',
        'right',
        'justify'
    ]
});
myNicEditor.panelInstance('feature-2');

jQuery('#commentForm .link-reset').click(function(){
    myNicEditor.removeInstance('feature-2');
    $('#feature-2').val('');
    myNicEditor.panelInstance('feature-2');
});
});


/////// feature 3

bkLib.onDomLoaded(function() {
var myNicEditor = new nicEditor({
    buttonList : [
        'bold',
        'italic',
        'underline',
        'forecolor',
        'left',
        'center',
        'right',
        'justify'
    ]
});
myNicEditor.panelInstance('feature-3');

jQuery('#commentForm .link-reset').click(function(){
    myNicEditor.removeInstance('feature-3');
    $('#feature-3').val('');
    myNicEditor.panelInstance('feature-3');
});
});



/////// feature 4
bkLib.onDomLoaded(function() {
var myNicEditor = new nicEditor({
    buttonList : [
        'bold',
        'italic',
        'underline',
        'forecolor',
        'left',
        'center',
        'right',
        'justify'
    ]
});
myNicEditor.panelInstance('feature-4');

jQuery('#commentForm .link-reset').click(function(){
    myNicEditor.removeInstance('feature-4');
    $('#feature-4').val('');
    myNicEditor.panelInstance('feature-4');
});
});



/////// feature 5

bkLib.onDomLoaded(function() {
var myNicEditor = new nicEditor({
    buttonList : [
        'bold',
        'italic',
        'underline',
        'forecolor',
        'left',
        'center',
        'right',
        'justify'
    ]
});
myNicEditor.panelInstance('feature-5');

jQuery('#commentForm .link-reset').click(function(){
    myNicEditor.removeInstance('feature-5');
    $('#feature-5').val('');
    myNicEditor.panelInstance('feature-5');
});
});


///////// feature 6
bkLib.onDomLoaded(function() {
var myNicEditor = new nicEditor({
    buttonList : [
        'bold',
        'italic',
        'underline',
        'forecolor',
        'left',
        'center',
        'right',
        'justify'
    ]
});
myNicEditor.panelInstance('feature-6');

jQuery('#commentForm .link-reset').click(function(){
    myNicEditor.removeInstance('feature-6');
    $('#feature-6').val('');
    myNicEditor.panelInstance('feature-6');
});
});



////// feature 7
bkLib.onDomLoaded(function() {
var myNicEditor = new nicEditor({
    buttonList : [
        'bold',
        'italic',
        'underline',
        'forecolor',
        'left',
        'center',
        'right',
        'justify'
    ]
});
myNicEditor.panelInstance('feature-7');

jQuery('#commentForm .link-reset').click(function(){
    myNicEditor.removeInstance('feature-7');
    $('#feature-7').val('');
    myNicEditor.panelInstance('feature-7');
});
});


/////// feature 8

bkLib.onDomLoaded(function() {
var myNicEditor = new nicEditor({
    buttonList : [
        'bold',
        'italic',
        'underline',
        'forecolor',
        'left',
        'center',
        'right',
        'justify'
    ]
});
myNicEditor.panelInstance('feature-8');

jQuery('#commentForm .link-reset').click(function(){
    myNicEditor.removeInstance('feature-8');
    $('#feature-8').val('');
    myNicEditor.panelInstance('feature-8');
});
});


//////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
//
//
//////// admin / intro / contents
bkLib.onDomLoaded(function() {
var myNicEditor = new nicEditor({
    buttonList : [
        'bold',
        'italic',
        'underline',
        'forecolor',
        'left',
        'center',
        'right',
        'justify'
    ]
});
myNicEditor.panelInstance('intro-main-content');

jQuery('#commentForm .link-reset').click(function(){
    myNicEditor.removeInstance('intro-main-content');
    $('#intro-main-content').val('');
    myNicEditor.panelInstance('intro-main-content');
});
});

//// admin / intro / signup

bkLib.onDomLoaded(function() {
    var myNicEditor = new nicEditor({
        buttonList : [
            'bold',
            'italic',
            'underline',
            'forecolor',
            'left',
            'center',
            'right',
            'justify'
        ]
    });
    myNicEditor.panelInstance('intro-signup-content');

    jQuery('#commentForm .link-reset').click(function(){
        myNicEditor.removeInstance('intro-signup-content');
        $('#intro-signup-content').val('');
        myNicEditor.panelInstance('intro-signup-content');
    });
});

/////// admin / subscribe / subscribe

bkLib.onDomLoaded(function() {
var myNicEditor = new nicEditor({
    buttonList : [
        'bold',
        'italic',
        'underline',
        'forecolor',
        'left',
        'center',
        'right',
        'justify'
    ]
});
myNicEditor.panelInstance('subscribe-content');

jQuery('#commentForm .link-reset').click(function(){
    myNicEditor.removeInstance('subscribe-content');
    $('#subscribe-content').val('');
    myNicEditor.panelInstance('subscribe-content');
});
});

