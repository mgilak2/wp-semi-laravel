<div class="title">
    
    شبکه های اجتماعی
    
</div>

<div class="field_text lightPlaceholder" style="width:100%">
    <label for="footer-social-one" class="label_title">توییتر : </label>
    {{ Form::text(['name'=>'footer[twitter]','id'=>'footer-social-one']) }}
</div>

<div class="field_text lightPlaceholder" style="width:100%">
    <label for="footer-social-two" class="label_title">فیسبوک : </label>
    {{ Form::text(['name'=>'footer[facebook]','id'=>'footer-social-two']) }}
</div>

<div class="field_text lightPlaceholder" style="width:100%">
    <label for="footer-social-three" class="label_title">گوگل پلاس : </label>
    {{ Form::text(['name'=>'footer[plus]','id'=>'footer-social-three']) }}
</div>
