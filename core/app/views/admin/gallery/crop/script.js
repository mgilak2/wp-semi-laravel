/*
6 button ba id cropButton
vojood dare ke ba click rooye oon
class cropWindow (admin.gallery.crop.holder) zaher mishe

too file holder ye class crops vojood dare
ke dar oon crop1 ta crop6 include shode
va crops ham display none hast

har dokme cropButton ye id dare be name
 crop1 ta crop6
 ke be komak in id
 be donbal crop to crops to file holder migardim
 .children("."+$(this).attr('id')+"-wrapper")
 crop1 ta crop6 ham hamashoon ye img tooshon hast
 ke jcrop be oonha bind shode
 class in img ha crop1 ta crop6 hast
 va jcrop ham donbale crop1 ta crop6 migarde ta
 ta kareso anjam bede
 bara tadakhol ijad nashodan be class haye crop1 ta crop6
 to file holder man -wrapper ro be akhare hamashoon
 ezafe kardam

 holder.blade.php
 <div class="crops" style="display: none">
 <div class="crop1-wrapper">
 @include('admin.gallery.crop.crop1')
 </div>
 </div>

 part1.blade.php
 <div id="crop1"  class="cropButton btn btn-primary">
 crop
 </div>

 */

$(document).ready(function(){
    var cropHandler = new CropHandler();

    cropHandler.init(cropHandler);
});

function CropHandler()
{
    this.button = $(".cropButton");

    this.holder = $('.cropWindow');

    this.submitButton = $(".submitButton");

    this.cropper = null;

    var cropperHandler = this;

    var currentThis = null;

    this.cropImagesUrl = [];

    this.init = function()
    {
        this.button.click(function(e)
        {
            cropperHandler.submitButton.hide();

            e.preventDefault();

            currentThis = $(this);

            var wrapper = $(".crops")
                .children("."+$(this).attr('id')+"-wrapper");

            var currentCropHolder = wrapper
                .children()
                .clone();

            cropperHandler.holder.children(".holdCrop").append(currentCropHolder);

            cropperHandler.holder.show();

            cropperHandler.cropper = new MyCropper();

            cropperHandler.cropper.init({
                selector:$(".holdCrop").children("img"),
                coordWrapper:$(".holdCrop").children(".coords"),
                coordiante:
                {
                    x1:$(".x1"),
                    x2:$(".x2"),
                    y1:$(".y1"),
                    y2:$(".y2"),
                    h:$(".h"),
                    w:$(".w")
                }
            });

        });

        var crop = $(".cropIt");

        crop.click(function(e)
        {
            cropperHandler.submitButton.show();

            e.preventDefault();
            currentThis.parent().children('.field_text').children('.coordination').val(
                "{"
                +'\"x1\":'+"\""+cropperHandler.cropper.coordiante.x1.val()+"\""
                +","
                +'\"x2\":'+"\""+cropperHandler.cropper.coordiante.x2.val()+"\""
                +","
                +'\"y1\":'+"\""+cropperHandler.cropper.coordiante.y1.val()+"\""
                +","
                +'\"y2\":'+"\""+cropperHandler.cropper.coordiante.y2.val()+"\""
                +","
                +'\"h\":'+"\""+cropperHandler.cropper.coordiante.h.val()+"\""
                +","
                +'\"w\":'+"\""+cropperHandler.cropper.coordiante.w.val()+"\""
                +"}"
            );

            cropperHandler.holder.children(".holdCrop").html("");

            cropperHandler.holder.hide();
        });

        var cancel = $('.cancelIt');

        cancel.click(function(e)
        {
            cropperHandler.submitButton.show();

            e.preventDefault();

            cropperHandler.holder.children(".holdCrop").html("");

            cropperHandler.holder.hide();

            cropperHandler.cropper = null;
        });
    }

}

/*
 *
 *  uses Jcrop for its functionality
 *
 *  param selector : is a jquery object of img
 *  param coords : is a jquery object of all coordinate wrapper
 */
function MyCropper()
{
    var  jcrop_api = null;
    this.selector = null;
    this.coordWrapper = null;
    this.coordiante = null;

    var cropper = this;

    this.setData = function(args)
    {
        this.selector = args.selector;
        this.coordWrapper = args.coordWrapper;
        this.coordiante = args.coordiante;
    };

    this.init = function(args)
    {
        this.setData(args);

        this.selector.Jcrop({
            onChange:   cropper.showCoords,
            onSelect:   cropper.showCoords,
            onRelease:  cropper.clearCoords
        },function()
        {
            jcrop_api = cropper.selector;
        });

        this.coordWrapper.on('change','input',function(e)
        {
            var x1 = cropper.coordiante.x1.val();
            var x2 = cropper.coordiante.x2.val();
            var y1 = cropper.coordiante.y1.val();
            var y2 = cropper.coordiante.y2.val();
            jcrop_api.setSelect([x1,y1,x2,y2]);
        });
    };
    
    this.showCoords = function(c)
    {
        cropper.coordiante.x1.val(c.x);
        cropper.coordiante.y1.val(c.y);
        cropper.coordiante.x2.val(c.x2);
        cropper.coordiante.y2.val(c.y2);
        cropper.coordiante.w.val(c.w);
        cropper.coordiante.h.val(c.h);
    };

    this.clearCoords = function()
    {
        cropper.coordWrapper.children('input').val('');
    }

}


