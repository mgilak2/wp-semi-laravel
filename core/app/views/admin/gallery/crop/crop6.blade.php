<img class="crop1" src="{{ $settings->gallery->featureImage6 }}" id="crop1" alt="[Jcrop Example]" />

<div id="coords" class="coords" style="display: none">
    <div class="inline-labels">
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
			<input type="submit" value="Crop Image" class="btn btn-large btn-inverse" />
    </div>
</div>
