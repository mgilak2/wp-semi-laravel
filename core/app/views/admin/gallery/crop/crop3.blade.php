<img class="crop1" src="{{ $settings->gallery->featureImage3 }}" id="crop1" alt="[Jcrop Example]" />

<div id="coords" class="coords" style="display: none">
    <div class="inline-labels">
        <label>X1 <input type="text" size="4" class="x1" name="x1" /></label>
        <label>Y1 <input type="text" size="4" class="y1" name="y1" /></label>
        <label>X2 <input type="text" size="4" class="x2" name="x2" /></label>
        <label>Y2 <input type="text" size="4" class="y2" name="y2" /></label>
        <label>W <input type="text" size="4" class="w" name="w" /></label>
        <label>H <input type="text" size="4" class="h" name="h" /></label>
    </div>
</div>
