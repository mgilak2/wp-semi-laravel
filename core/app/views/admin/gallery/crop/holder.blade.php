<script src="{{ asset('js/jquery.Jcrop.min.js') }}"></script>
<script src="{{ asset('core/app/views/admin/gallery/crop/script.js') }}"></script>

<div class="cropWindow" style="display: none">
    <div class="closeCrop"></div>

    <div class="holdCrop">

    </div>

    <div class="crops" style="display: none">
        <div class="crop1-wrapper">
            @include('admin.gallery.crop.crop1')
        </div>

        <div class="crop2-wrapper">
            @include('admin.gallery.crop.crop2')
        </div>

        <div class="crop3-wrapper">
            @include('admin.gallery.crop.crop3')
        </div>

        <div class="crop4-wrapper">
            @include('admin.gallery.crop.crop4')
        </div>

        <div class="crop5-wrapper">
            @include('admin.gallery.crop.crop5')
        </div>

        <div class="crop6-wrapper">
            @include('admin.gallery.crop.crop6')
        </div>

    </div>

    <div class="btn btn-danger cancelIt">close</div>
    <div class="btn btn-primary cropIt">crop</div>
</div>

<!--<script src="{{ asset('core/app/views/admin/gallery/crop/script.js') }}"></script>-->
