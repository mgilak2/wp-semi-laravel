<div class="item active">
    <div class="carousel-caption">
        <div class="title">
            پست اول
        </div>
<div class="galleryFormWrapper">

        <div class="field_text lightPlaceholder" style="z-index: 1000">
            <label for="gallery-crop-subject-1" class="label_title">عنوان:</label>
            {{ Form::text(['name'=>'gallery[cropSubject1]','id'=>'gallery-crop-subject-1']) }}
        </div>

        <div class="field_text field_textarea" style="margin-bottom:15px !important">
            {{ Form::long(['name'=>'gallery[cropContent1]','id'=>'gallery-crop-content-1']) }}
        </div>

        <div id="crop1"  class="cropButton btn btn-primary">
            crop
        </div>

        <div class="field_text lightPlaceholder coordWrapp" style="z-index: 1000">
            <label for="Coords1" class="label_title">مختصات:</label>
            {{ Form::text(['name'=>'gallery[coords1]','id'=>'Coords1','class'=>'coordination','style'=>'direction:ltr']) }}
        </div>


</div>

</div><!-- end carousel-caption -->

</div> <!-- end item -->
