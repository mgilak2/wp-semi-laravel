<div class="field_text lightPlaceholder" style="width:100%">
    <label for="gallery-subject" class="label_title">عنوان : </label>
    {{ Form::text(['name'=>'gallery[subject]','id'=>'gallery-subject']) }}
</div>

<div class="field_text lightPlaceholder" style="width:100%">
    <label for="gallery-ids" class="label_title">شماره مطالب : </label>
    {{ Form::text(['name'=>'gallery[ids]','id'=>'gallery-ids','style'=>'direction:ltr']) }}
    <span class="help-block">به طور مثال : 1,5,190,40,70,16 ;با کاما اعدا را جدا کنید</span>
</div>

<div class="field_text lightPlaceholder" style="width:100%">
    <label for="gallery-ids" class="label_title">بازیابی داده : </label>
    <input type="checkbox" name="gallery[fetchData]">
</div>

<div class="sliderWrapper">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">

        @include('admin.gallery.partials.post1')

        @include('admin.gallery.partials.post2')

        @include('admin.gallery.partials.post3')

        @include('admin.gallery.partials.post4')

        @include('admin.gallery.partials.post5')

        @include('admin.gallery.partials.post6')

    </div>



    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div> <!-- Carousel -->
</div>
@include('admin.gallery.crop.holder')
<script>
    $('.carousel').carousel({
        interval: 300000
    })
</script>

<!---->
<!--<div class="field_text lightPlaceholder" style="width:100%">-->
<!--    <label for="gallery-count-post" class="label_title">تعداد پست : </label>-->
<!--    {{ Form::text(['name'=>'galleryCountPost','id'=>'gallery-count-post']) }}-->
<!--</div>-->


