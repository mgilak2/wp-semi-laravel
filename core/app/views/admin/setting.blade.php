@include('admin.head')
    <div class="body-wrap" >
        <div class="content">
            <div class="container">
          

                    <div class="col-sm-12">
                        <!-- .row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- tabs -->
                                <div class="tabs-framed styled">
                                    <div class="inner">
                                        <ul class="tabs clearfix">
                                            <li class="active"><a href="#events" data-toggle="tab">مقدمه</a></li>
                                            <li><a href="#reminder" data-toggle="tab">ویژگی ها</a></li>
                                            <li><a href="#starred" data-toggle="tab">اپلیکیشن اندروید</a></li>
                                            <li><a href="#archive" data-toggle="tab">عضویت</a></li>
                                            <li><a href="#gallery" data-toggle="tab">مطالب اخیر</a></li>
                                            <li><a href="#testimonial" data-toggle="tab">نظر مشتریان</a></li>
                                            <li><a href="#price" data-toggle="tab">قیمت ها</a></li>
                                            <li><a href="#contact" data-toggle="tab">تماس با ما</a></li>
                                            <li><a href="#footer" data-toggle="tab">پایین صفحه</a></li>
                                        </ul>
                                        
                                        {{ Form::open(['method'=>'post','action'=>'','file'=>true],$matched) }}
                                        
                                        <div class="tab-content boxed">
                                            <div class="tab-pane fade in active clearfix" id="events">
                                                @include('admin.intro.intro')
                                            </div><!-- intro content end -->
                                            
                                            <div class="tab-pane fade clearfix" id="reminder">
                                                @include('admin.feature.feature')
                                            </div>
                                            
                                            <div class="tab-pane fade clearfix" id="starred">
                                               @include('admin.app.app')
                                            </div>

                                            <div class="tab-pane fade clearfix" id="archive">            
                                              @include('admin.subscribe.subscribe')
                                            </div>

                                            <div class="tab-pane fade clearfix" id="gallery">            
                                              @include('admin.gallery.gallery')
                                            </div>
                                            
                                            <div class="tab-pane fade clearfix" id="testimonial">            
                                              @include('admin.testimonial.testimonial')
                                            </div>
                        
                                            <div class="tab-pane fade clearfix" id="price">            
                                              @include('admin.price.price')
                                            </div>
                                                                                    
                                            <div class="tab-pane fade clearfix" id="contact">
                                              @include('admin.contact.contact')
                                            </div>

                                            <div class="tab-pane fade clearfix" id="footer">            
                                              @include('admin.footer.footer')
                                            </div>
                                            
                                        </div>

                                        {{ Form::submit(['value'=>'ثبت','class'=>'btn btn-primary submitButton','name'=>'settingSubmitted','style'=>'z-index:0']) }}

                                        {{ Form::close() }}
                                        
                                    </div>
                                </div>
                                <!--/ tabs -->
                            </div>
                            <br><br><br>
                            

                            
                    </div>
                    </div>
            </div><!-- / .container -->
        </div><!-- / .content -->
    </div>
