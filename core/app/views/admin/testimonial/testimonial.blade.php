<div class="field_text lightPlaceholder" style="width:100%">
    <label for="testimonial-subject" class="label_title">عنوان:</label>
    {{ Form::text(['name'=>'testimonial[subject]','id'=>'testimonial-subject']) }}
</div>


<div class="field_text lightPlaceholder" style="margin-bottom:0 !important;float: right">
    <label for="testimonial-count" class="label_title">تعداد نظرات : </label>
    {{ Form::text(['name'=>'testimonial[count]','id'=>'testimonial-count']) }}
</div>

<div class="field_text lightPlaceholder" style="margin-bottom:0 !important;float: right;width: 100%;margin-top: 10px">
    <label for="testimonialBackground" class="label_title">پسزمینه:</label>
    {{ Form::file(['name'=>'testimonial[background]','class'=>'file-input','id'=>'testimonialBackground']) }}
</div>