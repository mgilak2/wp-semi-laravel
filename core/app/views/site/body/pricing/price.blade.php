<!-- PRICES -->
        <section id="prices" data-anchor="prices" class="block">
            <div class="container">
            
                <!-- Title -->
                <header class="title">
                    <div class="circle"></div>
                    <h2 class="fit-h2" style="font-family: yekan">فروشگاه</h2>
                    <div class="line"></div>
                </header>
                
                <div class="row animate fadeIn-onload">
                        
                    <!-- Pricing Item 1 -->
                    <div class="col-sm-4 pricing-item">
                        <div class="pricing-title">
                            Basic
                        </div>
                        <div class="price">
                            <span>Free</span>
                            For Personal Use
                        </div>
                        <ul>
                            <li>Site Explorer Platform</li>
                            <li>Fresh Index Each Month</li>
                            <li>Access to API</li>
                            <li>60 Detailed Reports Per Month</li>
                            <li>2,000,000 Analyzable Backlinks</li>
                        </ul>
                        <a href="#" class="btn btn-primary">Subscribe</a>
                    </div>
                            
                    <!-- Pricing Item 2 -->
                    <div class="col-sm-4 pricing-item best">
                        <div class="pricing-title">
                            Plus
                        </div>
                        <div class="price">
                            <span>$9.99<span>/mo</span></span>
                            For Business
                        </div>
                        <ul>
                            <li>Site Explorer Platform</li>
                            <li>Fresh Index Each Week</li>
                            <li>Access to API</li>
                            <li>120 Detailed Reports Per Month</li>
                            <li>5,000,000 Analyzable Backlinks</li>
                        </ul>
                        <a href="#" class="btn btn-primary">Subscribe</a>
                    </div>
                            
                    <!-- Pricing Item 3 -->
                    <div class="col-sm-4 pricing-item">
                        <div class="pricing-title">
                            Maximum
                        </div>
                        <div class="price">
                            <span>$99.99</span>
                            Corporate Offer
                        </div>
                        <ul>
                            <li>Site Explorer Platform</li>
                            <li>Fresh Index Each Day</li>
                            <li>Access to API</li>
                            <li>60 Detailed Reports Per Month</li>
                            <li>8,000,000 Analyzable Backlinks</li>
                        </ul>
                        <a href="#" class="btn btn-primary">Subscribe</a>
                    </div>
                            
                </div><!-- /row -->
                
            </div><!-- /container -->
        </section>
        <!-- END PRICES -->