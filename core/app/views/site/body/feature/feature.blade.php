<!-- FEATURES SECTION -->
        <section id="features-section" data-anchor="features-section">
        	<div class="heading">
            	<h3>{{ $feature->subject }}</h3>
            </div>

            <!-- feature -->
        	<div class="col-sm-3 features-item one">

                <i class="fa fa-mobile big-icon"></i>
				<i class="fa fa-mobile big-small"></i>
                <div class="description">
                	<h5>{{ $feature->subject1 }}</h5>
                    <span>{{ $feature->content1 }}</span>
                </div>

            </div>

            <!-- feature -->
        	<div class="col-sm-3 features-item two">

                <i class="fa fa-user big-icon"></i>
				<i class="fa fa-user big-small"></i>
                <div class="description">
                	<h5>{{ $feature->subject2 }}</h5>
                    <span>{{ $feature->content2 }}</span>
                </div>

            </div>

            <!-- feature -->
        	<div class="col-sm-3 features-item one">

                <i class="fa fa-camera big-icon"></i>
				<i class="fa fa-camera big-small"></i>
                <div class="description">
                	<h5>{{ $feature->subject3 }}</h5>
                    <span>{{ $feature->content3 }}</span>
                </div>

            </div>

            <!-- feature -->
        	<div class="col-sm-3 features-item two">

                <i class="fa fa-microphone big-icon"></i>
				<i class="fa fa-microphone big-small"></i>
                <div class="description">
                	<h5>{{ $feature->subject4 }}</h5>
                    <span>{{ $feature->content4 }}</span>
                </div>

            </div>

            <!-- feature -->
        	<div class="col-sm-3 features-item two">

                <i class="fa fa-lightbulb-o big-icon"></i>
				<i class="fa fa-lightbulb-o big-small"></i>
                <div class="description">
                	<h5>{{ $feature->subject5 }}</h5>
                    <span>{{ $feature->content5 }}</span>
                </div>

            </div>

            <!-- feature -->
        	<div class="col-sm-3 features-item one">

                <i class="fa fa-lock big-icon"></i>
				<i class="fa fa-lock big-small"></i>
                <div class="description">
                	<h5>{{ $feature->subject6 }}</h5>
                    <span>{{ $feature->content6 }}</span>
                </div>

            </div>

            <!-- feature -->
        	<div class="col-sm-3 features-item two">

                <i class="fa fa-heart big-icon"></i>
				<i class="fa fa-heart big-small"></i>
                <div class="description">
                	<h5>{{ $feature->subject7 }}</h5>
                    <span>{{ $feature->content7 }}</span>
                </div>

            </div>

            <!-- feature -->
        	<div class="col-sm-3 features-item one">

                <i class="fa fa-star big-icon"></i>
				<i class="fa fa-star big-small"></i>
                <div class="description">
                	<h5>{{ $feature->subject8 }}</h5>
                    <span>{{ $feature->content8 }}</span>
                </div>

            </div>

        </section>
        <!-- END FEATURES SECTION -->