<!-- CONTACT -->
        <section id="contact" data-anchor="contact" class="block dark">
            <div class="container">
                
                <!-- Title -->
                <header class="title">
                    <div class="circle"></div>
                    <h2 class="fit-h2" style="font-family: yekan">{{ $contact->subject }}</h2>
                    <div class="line"></div>
                </header>
                
                <!-- Subtext for contact -->
                <div class="subtext contact-content">
                    {{ $contact->content }}
                </div>
                
                <!-- Contact Form -->
                <div class="loader"></div>
				<div class="bar"></div>
                <form action="http://www.asvada.com/themes/applum/mail.php" role="form" class="animate slideUp-onload contactForm" name="cform" method="post">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-2 form-group">
                            <input type="email" name="email" id="e-mail" class="form-control" placeholder="Email Address">
                        </div>
                        <div class="col-md-4 form-group">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Your Name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 form-group">
                            <textarea id="message" name="message" class="form-control" rows="5" placeholder="Message"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 form-group">
                            <input type="submit" name="submit" class="submit btn btn-primary" value="{{ $contact->button }}">
                        </div>
                    </div>
                    <div class="row name-missing">
                    	<div class="col-md-8 col-md-offset-2">
                    		<div class="alert alert-danger">All Fields are Required!</div>
                    	</div>
                    </div>
                </form>
                
            </div><!-- /container -->
        
        </section>
        <!-- END CONTACT -->