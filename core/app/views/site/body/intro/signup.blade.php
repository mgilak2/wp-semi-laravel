<!-- Head Bottom Section -->
<div class="head-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-4 text-center">
                <a href="#subscribe" data-scroll="subscribe" class="btn btn-primary">{{ $intro->signupButton }}</a>
            </div>

            <div class="col-md-8" style="direction: rtl;">
                <h2 class="intro-signup-subject">{{ $intro->signupSubject }}</h2>
                <p class="intro-signup-content">
                    {{ $intro->signupContent }}
                </p>
            </div>

        </div><!-- /row -->
    </div><!-- /container -->
</div><!-- /head-bottom -->