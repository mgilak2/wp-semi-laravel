<style>
.parallax-head {
    background: url({{ $intro->background }}) no-repeat fixed 50% 0;
}
</style>
<!-- HEAD SECTION -->
        <section id="head-section" data-anchor="head-section" class="parallax-head parallax-background">
            
            <!-- Color Overlay -->
            <div class="color-overlay"></div>
            
            <!-- Overlay Inner -->
            <div class="overlay-inner">
                <div id="head-section-content" class="container">
                    <div class="row">
                    
                        <!-- Icons Image -->
                        <div class="head-info col-md-3 animate slideRight">
                            <img src="{{ $intro->network }}" alt="" />
                        </div>
                        
                        <!-- Head Text -->
                        <div class="head-info col-md-5 head-info animate slideDown">
                            <h1>{{ $intro->subject }}</h1>
                            <p>
                                {{ $intro->content }}
                            </p>
                            <p><a href="#features" data-scroll="features" class="btn btn-dark">{{ $intro->contentButton }}</a></p>
                            
                            <!-- Stores Images -->
                            <div class="stores">
                                <a href="#"><img src="{{$intro->icon1}}" alt="" /></a>
                                <a href="#"><img src="{{$intro->icon2}}" alt="" /></a>
                                <a href="#"><img src="{{$intro->icon3}}" alt="" /></a>
                            </div>
                            
                        </div><!-- /head-info -->
                        
                        <!-- App Snapshot -->
                        <div class="col-md-4">
                            <img class="animate slideLeft" src="{{ $intro->iphone }}" alt="" />
                        </div>
                        
                    </div><!-- /row -->
                </div><!-- /container -->
                
               @include('site.body.intro.signup')
                
            </div><!-- /overlay-inner -->
        </section><!-- /head-section -->
        <!-- END SECTION -->