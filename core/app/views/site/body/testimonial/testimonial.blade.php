<style>
    .parallax-testimonials {
        background: url({{ $testimonial->background }}) no-repeat fixed 50% 0;
    }
</style>
<!-- TESTIMONIALS -->
        <section id="testimonials" data-anchor="testimonials" class="block parallax-testimonials parallax-background">
        
            <!-- Color Overlay -->
            <div class="color-overlay"></div>
            
            <!-- Overlay Inner -->
            <div class="overlay-inner">
            
                <!-- Title -->
                <header class="title dark">
                    <div class="circle"></div>
                    <h2 class="fit-h2" style="font-family: yekan">{{ $testimonial->subject }}</h2>
                    <div class="line"></div>
                </header>
                
                <div class="container animate fadeIn-onload">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <!--BX Slider -->
                            <div id="testimonials-slides">
                                
                                <!-- Slide 1 -->
                                <div>
                                    <img src="{{ asset('images/faces/face1.jpg') }}" class="img-circle" alt="" />
                                    <div class="testimonial">
                                        Nulla hendrerit diam sit amet purus dignissim bibendum ac eu urna. Praesent convallis lectus in vehicula scelerisque. Mauris fermentum sodales sodales. Etiam tempor pharetra quam at viverra. Vestibulum nibh dui, fermentum vel leo in, vehicula ultricies felis. In dignissim dolor augue, ac pharetra nibh venenatis in. Etiam suscipit mauris ac.
                                    </div>
                                    <div class="client-name">Tony Stark</div>
                                </div>
                                    
                                <!-- Slide 2 -->
                                <div>
                                    <img src="{{ asset('images/faces/face2.jpg') }}" class="img-circle" alt="" />
                                    <div class="testimonial">
                                        Phasellus quis euismod velit. Suspendisse potenti. Etiam massa metus, gravida vel consectetur ac, lobortis et dui. Sed a lectus eros. Vivamus ut leo aliquet, molestie felis nec, pulvinar mauris. Proin dignissim sagittis justo, sed gravida ipsum facilisis quis. Nunc aliquet.
                                    </div>
                                    <div class="client-name">Mark Zuckerberg</div>
                                </div>
                                    
                                <!-- Slide 3 -->
                                <div>
                                    <img src="{{  asset('images/faces/face3.jpg') }}" class="img-circle" alt="" />
                                    <div class="testimonial">
                                        Vivamus luctus mauris quis massa imperdiet accumsan. In fringilla, felis quis pharetra lobortis, risus nisl ullamcorper dolor, ut varius leo tortor a elit. Suspendisse interdum, tellus ut aliquam gravida, velit lorem commodo tortor, pulvinar faucibus nunc odio sit amet ligula. Mauris faucibus sit amet magna.
                                    </div>
                                    <div class="client-name">Rebecca Tinkoff</div>
                                </div>
                                    
                            </div><!-- /testimonials-slides -->
                        </div><!-- /col-md-12 -->
                    </div><!-- /row -->
                        

                        
                </div><!-- /container -->
            </div><!-- /overlay-inner -->
        </section>
        <!-- END TESTIMONIALS -->