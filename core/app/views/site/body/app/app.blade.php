<style>
    #more-info {
        background: url({{ $app->background }}) no-repeat top center #f9fafb;
    }
</style>
<!-- MORE INFO SECTION -->
        <section id="more-info">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-push-6 direction">
                        <h3>{{ $app->subject }}</h3>
                        
                        <!-- Description Text -->
                        <p>
                            {{ $app->content }}
                        </p>

                        <!-- OS icons -->
                        <p>
                            <i class="fa fa-apple fa-lg"></i>
                            <i class="fa fa-android fa-lg"></i>
                            <i class="fa fa-windows fa-lg"></i>
                        </p>
                        
                        <!-- Buttons -->
                        <a href="#subscribe" data-scroll="subscribe" class="btn btn-primary">{{ $app->button1 }}</a>
                        <a href="#" class="btn btn-dark">{{ $app->button2 }}</a>
                    </div>
                    <div class="col-md-6 col-md-pull-6">
                        <img src="{{ $app->tablet }}" alt="" class="tablet animate slideLeft-onload" />
                        <img src="{{ $app->iphone }}" alt="" class="phone-overlay animate slideRight-onload" />
                    </div>
                </div><!-- /row -->
            </div><!-- /container -->
        </section>
        <!-- END MORE INFO SECTION -->