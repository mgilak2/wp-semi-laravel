<!-- NAVIGATION -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="top-menu">
            <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#top" data-scroll="top"><img src="{{asset('images/logo.png')}}" alt="" /></a>
                    </div>
              
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          
                    <ul class="nav navbar-nav navbar-right standardNav">
                    	<li><a href="#head-section" data-scroll="head-section"></a></li>
                        <li><a href="#features-section" data-scroll="features-section">ویژگی ها</a></li>
                        <li><a href="#gallery" data-scroll="gallery">مطالب اخیر</a></li>
                        <li><a href="#testimonials" data-scroll="testimonials">نظر مشتریان</a></li>
                        <li><a href="#prices" data-scroll="prices">فروشگاه</a></li>
                        <li><a href="#contact" data-scroll="contact">تماس با ما</a></li>
                        <li><a href="http://themeforest.net/" target="_blank" class="purchase"><i class="fa fa-thumbs-o-up"></i> فروشگاه</a></li>
                    </ul>
                </div><!-- /navbar-collapse -->
            </div><!-- /container -->
        </nav><!-- /vavbar -->
        <!-- END NAVIGATION -->