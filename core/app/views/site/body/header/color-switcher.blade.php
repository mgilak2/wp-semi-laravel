<!-- START COLOR SWITCHER -->
        <div class="style-settings dropdown">
          <a data-toggle="dropdown" class="style-settings-icon" href="#"><i class="fa fa-cog"></i></a>
          <div class="dropdown-menu" role="menu">
              <div class="inner">
                  <a class="switchbox" id="yellow"></a>
                  <a class="switchbox" id="red"></a>
                  <a class="switchbox" id="green"></a>
                  <a class="switchbox" id="mediumaquamarine"></a>
                  <a class="switchbox" id="blue"></a>
                  <a class="switchbox" id="pink"></a>
                  <a class="switchbox" id="fiolet"></a>
                  <a class="switchbox" id="black"></a>
                  <div class="clearfix"></div>
              </div>
          </div>
        </div>
        <!-- END COLOR SWITCHER -->
    
