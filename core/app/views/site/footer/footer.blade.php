 <!-- FOOTER -->
        <section id="footer" class="block dark">
            <div class="container">
                <div class="row">
                
                    <!-- Footer Left Column -->
                    <div class="col-md-6">
                        <p>Copytight &copy; 2014 Applum</p>
                        
                        <!-- Social Icons -->
                        <div class="social-icons">
                            <ul>
<!--                                <li class="twitter">-->
<!--                                    <a href="#" target="_blank">Twitter</a>-->
                                <li style="border: none">
                                    <img style="width: 60px" src="{{ asset('images/facebook_hex_icon.png') }}" alt="" />
                                </li>
                                <li style="border: none">
                                    <img style="width: 60px" src="{{ asset('images/twitter_hex_icon.png') }}" alt="" />
                                </li>

                            </ul>
                        </div><!-- /social-icons -->
                        <div class="clearfix"></div>
                        
                    </div><!-- /col-md-6 -->
                    
                    <!-- Stores Images Links -->
                    <div class="col-md-6 stores">
                        <a href="#"><img src="{{ $footer->icon1 }}" alt="" /></a>
                        <a href="#"><img src="{{ $footer->icon2 }}" alt="" /></a>
<!--                        <a href="#"><img src="{{ asset('images/.png') }}" alt="" /></a>-->
                    </div><!-- /col-md-6 -->
                    
                </div><!-- /row -->
            </div><!-- /container -->
        </section>
        <!-- END FOOTER -->

        <!-- Include all compiled plugins below, or include individual files as needed -->
        <script type="text/javascript" src="{{ asset('js/jquery.queryloader2.min.js') }}"></script><!-- Page Loading -->
        <script type="text/javascript" src="{{ asset('js/jquery.bxslider.min.js') }}"></script><!-- Page Loading -->
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script><!-- bootstrap -->
        <script type="text/javascript" src="{{ asset('js/classie.js') }}"></script><!-- For Animated Header -->
		<script type="text/javascript" src="{{ asset('js/cbpAnimatedHeader.js') }}"></script><!-- Animated Header -->
        <script type="text/javascript" src="{{ asset('js/jquery.parallax-1.1.3.js') }}"></script><!-- Parallax -->
        <script type="text/javascript" src="{{ asset('js/jquery.fittext.js') }}"></script><!-- Fit Texts (Responsive Headings) -->
        <script type="text/javascript" src="{{ asset('js/contact.js') }}"></script><!-- Ajax Contact Form -->
        <script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script><!-- Owl Slider for Gallery and Testimonials -->
        <script type="text/javascript" src="{{ asset('js/styleswitch.js') }}"></script><!-- Scripts -->
        <script type="text/javascript" src="{{ asset('js/placeholders.min.js') }}"></script><!-- IE Placeholder Hack -->
        <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script><!-- Scripts -->
	</body>
</html>