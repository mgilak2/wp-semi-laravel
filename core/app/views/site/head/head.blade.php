<!DOCTYPE html>
<html>
	<head>
        <meta charset="UTF-8">
        <title>Applum - Responsive App Showcase Landing Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="A Template by Asvada Themes"/>
        <meta name="keywords" content="HTML, CSS, JavaScript, Ajax, Bootstrap, Parallax, Jquery, app, landing, landing page, responsive"/>
        <meta name="author" content="Asvada Themes"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        
        <!-- Styles -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" id="bootstrap"><!-- Bootstrap CSS -->
        <link href="{{ asset('css/social.css') }}" rel="stylesheet" id="social-icons"><!-- Social Icons -->
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" id="font-awesome"><!-- Font Awesome -->
        <link href="{{ asset('css/animations.css') }}" rel="stylesheet" id="animations"><!-- Animations -->
        <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet"><!-- Owl Slider for Gallery and Testimonials -->

        <link href="{{ asset('css/style.css') }}" rel="stylesheet" id="global-styles"><!-- Global Styles -->
        <link href="{{ asset('css/themes/yellow.css') }}" rel="stylesheet" id="color-theme" title="styles1"><!-- Color Theme -->

        <!-- jQuery Plugin -->
        <script type="text/javascript" src="{{ asset('js/jquery-latest.min.js') }}"></script>

        <link href="{{ asset('css/color-switcher.css') }}" rel="stylesheet" id="color-switcher"><!-- Color Switcher -->
        <link rel="stylesheet" id="switch_scheme" href="#"><!--Color Scheme Picker-->
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

</head>

<body data-spy="scroll" data-target="#top-menu" id="top" data-anchor="top">