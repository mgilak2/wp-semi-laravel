<!DOCTYPE html>
<html>
	<head>
        <meta charset="UTF-8">
        <title>Applum - Responsive App Showcase Landing Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="A Template by Asvada Themes"/>
        <meta name="keywords" content="HTML, CSS, JavaScript, Ajax, Bootstrap, Parallax, Jquery, app, landing, landing page, responsive"/>
        <meta name="author" content="Asvada Themes"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Styles -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" id="bootstrap"><!-- Bootstrap CSS -->
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" id="font-awesome"><!-- Font Awesome -->
        <link href="{{ asset('css/animations.css') }}" rel="stylesheet" id="animations"><!-- Animations -->

        <link href="{{ asset('css/style.css') }}" rel="stylesheet" id="global-styles"><!-- Global Styles -->
        <link href="{{ asset('css/themes/yellow.css') }}" rel="stylesheet" id="color-theme" title="styles1"><!-- Color Theme -->

        <!-- jQuery Plugin -->
        <script type="text/javascript" src="{{ asset('js/jquery-latest.min.js') }}"></script>

        <link rel="stylesheet" id="switch_scheme" href="#"><!--Color Scheme Picker-->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

</head>

<body data-spy="scroll" data-target="#top-menu" id="top" data-anchor="top">

<link rel="stylesheet" href="{{asset("css/users/style.css")}}"/>
<style>
#more-info {
    background: url({{ asset("images/geometric.jpg") }}) no-repeat top center #f9fafb;
    height: 100%;
    background-size: cover;
}
</style>
<!-- MORE INFO SECTION -->
        <div class="sidebar">
            <ul>
                <li>
                    <img style="width: 95%" src="{{asset("images/icons/home_icon.png")}}" alt=""/>
                    <p>
                        خانه
                    </p>
                </li>

                <li>
                    <img style="width: 77%;right: 28%" src="{{asset("images/icons/setting_icon.png")}}" alt=""/>
                    <p style="right: 19px;">
تنظیمات
                    </p>
                </li>
            </ul>

        </div>

        <form method="POST" action="/profile" enctype="multipart/form-data">
        <section id="more-info" >
            <div class="container" style="position:relative;left: 36px;background-color: rgba(255,255,255,0.5)">
                <div class="row">

                    <div class="col-md-4" >
                        <div class="title">
                            اطلاعات زنبورداری
                        </div>

                        <div class="form-group">
                            <label for="">
تعداد کندو ها
                            </label>
                            <input type="text" value="{{$usermeta->hive_count}}" name="hive_count" class="form-control"/>
                        </div>


                        <div class="form-group">
                            <label for="">
نژاد زنبور
                            </label>
                            <select name="race_id" class="form-control" id="">
                                <option value="0">انتخاب کنید</option>
                                <option value="1">کارنیولان</option>
                                <option value="2">کارنیکا</option>
                                <option value="3">قفقازی</option>
                                <option value="4">ایرانی</option>
                                <option value="5">بومی</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">
تامین ملکه ی مورد نیاز زنبورستان
                            </label>
                            <select name="quean" class="form-control" id="">
                                <option value="0">انتخاب کنید</option>
                                <option value="1">خرید از مراکز تولید ملکه</option>
                                <option value="2">تولید ملکه مورد نیاز زنبورستان توسط خود زنبور دار</option>
                                <option value="3">جایگزینی ملکه به صورت طبیعی</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">
                                محل استقرار زنبورستان
                            </label>
                            <input type="text" name="location" value="{{$usermeta->location}}" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="">
زنبورستان تحت پوشش صندوق بیمه ی محصولات کشاورزی میباشید ؟
                            </label>
                            <div class="ticks">
                                <label for="">
                                                                بله
                                    <input type="radio" name="insurance"/>
                                </label>

                                <label for="">
    خیر
                                    <input type="radio" name="insurance"/>
                                </label>
                            </div>

                        </div>

                    <div class="form-group">
                        <label for="">
                            روش زنبوردار
                        </label>
                        <select name="technique_id" class="form-control" id="">
                            <option value="0">انتخاب کنید</option>
                            <option value="1">کوچ رو بیرون از استان</option>
                            <option value="2">کوچ رو درون استان</option>
                            <option value="3">ثابت</option>
                            <option value="4">ایستگاه پرورش ملکه</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">
سایر تولیدات و فراورده ها و فعالیت های خاص زنبور دار
                        </label>

                        <div style="margin-top: 10px;width: 100%;height: 32px">
                            <label style="float: right;width: 96px" for="">
انتخاب کنید
                            </label>
                            <input style="float: right;" type="checkbox" name="show" class="showOtherActivities"/>
                        </div>

                        <div class="row otherActivities deactive" style="display: none">
                                <div class="checkboxItem">
                                        <label for="">
                                            تولید ملکه
                                        </label>
                                        <input name="practice[]" value="1" type="checkbox"/>
                                </div>

                                <div class="checkboxItem">
                                    <label for="">
    تولید گرده گل
                                    </label>
                                    <input type="checkbox" name="practice[]" value="2" />
                                </div>

                                <div class="checkboxItem">
                                    <label for="">
    تولید موم
                                    </label>
                                    <input type="checkbox" name="practice[]" value="3" />
                                </div>

                                <div class="checkboxItem">
                                    <label for="">
    تولید ژل رویال
                                    </label>
                                    <input type="checkbox" name="practice[]" value="4" />
                                </div>

                              <div class="checkboxItem">
                                  <label for="">
    تولید بره موم
                                  </label>
                                  <input type="checkbox" name="practice[]" value="5" />
                              </div>

                                <div class="checkboxItem">
                                    <label for="">
    تولید زهر
                                    </label>
                                    <input type="checkbox" name="practice[]" value="6" />
                                </div>

                            <div class="checkboxItem">
                              <label for="">
انتشار نشریات و مقالات علمی
                              </label>
                                  <input type="checkbox" name="practice[]" value="7" />
                            </div>

                            <div class="checkboxItem">
                              <label for="">
ایجاد سایت آموزشی زنبور داری
                              </label>
                                  <input type="checkbox" name="practice[]" value="8" />
                            </div>

                            <div class="checkboxItem">
                              <label for="">
ساخت فیلم زنبور داری
                              </label>
                                  <input type="checkbox" name="practice[]" value="9" />
                            </div>

                            <div class="checkboxItem">
                              <label for="">
اختراع ابزار و لوازم زنبور داری
                              </label>
                                  <input type="checkbox" name="practice[]" value="10" />
                            </div>
                    </div>
</div>
<input type="submit" class="btn btn-primary" name="profile_submit" value="ثبت"/>
                    </div> <!-- enc col-md-4 -->

                    <div class="col-md-4">
                        <div class="title">
                            اطلاعات شغلی
                        </div>

                        <div class="form-group">
                            <label for="">
عضو شرکت تعاونی هستید ؟
                            </label>
                            <div class="ticks cooperative">
                                <label for="">
                                                                بله
                                    <input data-slideup="true" type="radio" name="cooperative_member"/>
                                </label>

                                <label for="">
    خیر
                                    <input data-slideup="false" type="radio" name="cooperative_member"/>
                                </label>
                            </div>

                        </div>

                        <div class="cooperativeActor deactive" style="display: none">
                            <div class="form-group">
                                <label for="">
نام شرکت تعاونی
                                </label>
                                <input type="text" value="{{$usermeta->cooperative_name}}" name="cooperative_name" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">
                                کد شناسنامه زنبور داری
                            </label>
                            <input type="text" value="{{$usermeta->beekeeping_book_code}}" name="beekeeping_book_code" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="">
عضو اتحادیه زنبور داران هستید ؟
                            </label>
                            <div class="ticks union" >
                                <label for="">
                                                                بله
                                    <input type="radio" data-slideup="true" name="union_beekeeper_member"/>
                                </label>

                                <label for="">
    خیر
                                    <input type="radio" data-slideup="false" name="union_beekeeper_member"/>
                                </label>
                            </div>

                        </div>

                        <div class="unionActor deactive" style="display: none">
                            <div class="form-group">
                                <label for="">
نام اتحادیه
                                </label>
                                <input type="text" name="union_name" value="{{$usermeta->union_name}}" class="form-control"/>
                            </div>
                        </div>

                            <div class="form-group">
                                <label for="">
                                    وضعیت شغلی زنبور دار
                                </label>
                                <select name="job_id" id="" class="form-control">
                                    <option value="1" selected="selected">انتخاب کنید</option>
                                    <option value="2">به عنوان شغل اصلی</option>
                                    <option value="3">به عنوان شغل دوم</option>
                                    <option value="4">به عنوان شغل جانبی و تفریحی</option>
                                </select>
                            </div>

                        <div class="form-group">
                            <label for="">
میزان تجربه ی زنبور دار بر حسب سال
                            </label>
                            <input type="text" name="year_record" value="{{$usermeta->year_record}}" class="form-control"/>
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<label for="">--}}
{{--چند ماه سابقه ؟--}}
                            {{--</label>--}}
                            {{--<input type="text" class="form-control" name="month_record"/>--}}
                        {{--</div>--}}
                    </div>

                    <div class="col-md-4">

                        <div class="title">
                            اطلاعات شخصی
                        </div>

                        <div class="form-group">
                            <label for="">
                                نام
                            </label>
                            <input name="name" value="{{$usermeta->name}}" type="text" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="">
نام خانوادگی
                            </label>
                            <input name="lastName" value="{{$usermeta->lastName}}" type="text" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="">
کشور
                            </label>
                            <select name="country_id" id="" class="form-control">
                                <option value="0">انتخاب کنید</option>
                                @foreach($countries as $id => $country)
                                    <option value="{{$id}}">{{$country}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">
استان
                            </label>
                            <select name="province_id" id="" class="form-control">
                                <option value="0">انتخاب کنید</option>
                                @foreach($provinces as $id => $province)
                                    <option value="{{$id}}">{{$province}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">
شهر
                            </label>
                            <select name="city_id" id="" class="form-control">
                                <option value="0">انتخاب کنید</option>
                                @foreach($cities as $id => $city)
                                    <option value="{{$id}}">{{$city}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">
بخش یا روستا
                            </label>
                            <input type="text" name="village" value="{{$usermeta->village}}" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="">
                                شماره پستی
                            </label>
                            <input class="form-control" type="text" value="{{$usermeta->post_code}}" name="post_code"/>
                        </div>

                        <div class="form-group">
                            <label for="">
تلفن ثابت
                            </label>
                            <input name="phone" value="{{$usermeta->phone}}" type="text" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="">
تلفن همراه
                            </label>
                            <input name="cellphone" value="{{$usermeta->cellphone}}" type="text" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="">
ایمیل
                            </label>
                            <input name="email" value="{{$usermeta->email}}" type="text" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="">
میزان تحصیلات
                            </label>
                            <select name="education_id" class="form-control" id="">
                                <option value="1">انتخاب کنید</option>
                                <option value="2">دکتری</option>
                                <option value="3">لیسانس و بالاتر</option>
                                <option value="4">دیپلم و فوق دیپلم</option>
                                <option value="5">کمتر از دیپلم</option>
                                <option value="6">بی سواد</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">
عکس
                            </label>
                            <input name="picture" type="file" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="">
اطلاعات بیشتر
                            </label>
                            <textarea style="color: black" name="description" cols="30" rows="10" class="form-control">
                             @if(!is_null($usermeta->description) and strlen($usermeta->description)>0)
                                 {{$usermeta->description}}
                             @else
شرح فعالیت ها - تحقیقات - محصولات و ...
                             @endif
                            </textarea>
                        </div>

                    </div>

                </div><!-- /row -->
            </div><!-- /container -->
            <div style="height: 38px"></div>
        </section>
        <!-- END MORE INFO SECTION -->

        </form>


<!-- Include all compiled plugins below, or include individual files as needed -->
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script><!-- bootstrap -->
        <script type="text/javascript" src="{{asset("js/users/custom.js")}}"></script>
	</body>
</html>