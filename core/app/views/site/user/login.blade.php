<!DOCTYPE html>
<html>
	<head>
        <meta charset="UTF-8">
        <title>Applum - Responsive App Showcase Landing Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="A Template by Asvada Themes"/>
        <meta name="keywords" content="HTML, CSS, JavaScript, Ajax, Bootstrap, Parallax, Jquery, app, landing, landing page, responsive"/>
        <meta name="author" content="Asvada Themes"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Styles -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" id="bootstrap"><!-- Bootstrap CSS -->
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" id="font-awesome"><!-- Font Awesome -->
        <link href="{{ asset('css/animations.css') }}" rel="stylesheet" id="animations"><!-- Animations -->

        <link href="{{ asset('css/style.css') }}" rel="stylesheet" id="global-styles"><!-- Global Styles -->
        <link href="{{ asset('css/themes/yellow.css') }}" rel="stylesheet" id="color-theme" title="styles1"><!-- Color Theme -->

        <!-- jQuery Plugin -->
        <script type="text/javascript" src="{{ asset('js/jquery-latest.min.js') }}"></script>

        <link rel="stylesheet" id="switch_scheme" href="#"><!--Color Scheme Picker-->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

</head>

<body data-spy="scroll" data-target="#top-menu" id="top" data-anchor="top">

<style>
    .parallax-subscribe
    {
        background: url({{ asset("images/backgrounds/subscribe.jpg") }}) no-repeat fixed 50% 0;
    }

    #result
    {
        text-align: center;
        font-family: yekan;
        font-size: 20px;
        display: inherit;
        position: relative;
    }
</style>
<!-- SUBSCRIBE -->
<section id="subscribe" data-anchor="subscribe" class="block-huge dark parallax-subscribe parallax-background">

    <!-- Dark Overlay-->
    <div class="dark-overlay"></div>

    <!-- Overlay Inner -->
    <div class="overlay-inner">
        <div class="container animate slideUp">
            <h2  class="text-center fit-h2" style="font-family: yekan">صفحه ی ورود</h2>

            <!-- Subscription Form -->
            <form action="{{$_SERVER['REQUEST_URI']}}" id="invite" method="POST" role="form" class="row animate fadeIn">

                <div class="col-md-4 col-md-offset-4 form-group">
                    <input type="text" class="form-control" placeholder="User Name" name="user_login" id="address" data-validate="validate(required, email)">
                </div>

                <div class="col-md-4 col-md-offset-4 form-group">
                    <input type="password" class="form-control" placeholder="Password" name="user_password" id="address" data-validate="validate(required, email)">
                </div>

                <div class="col-md-4 col-md-offset-4 form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <button style="font-family: tahoma" type="submit" class="btn btn-primary btn-block">ورود</button>
                    </div>
                </div>

            </form>


        </div><!-- /container -->
    </div><!-- /overlay-inner -->
</section>
<!-- END SUBSCRIBE -->

<!-- Include all compiled plugins below, or include individual files as needed -->
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script><!-- bootstrap -->
        <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script><!-- Scripts -->
	</body>
</html>