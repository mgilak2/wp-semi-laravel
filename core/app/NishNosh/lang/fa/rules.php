<?php

return array(
    'required'      => 'The :attribute field is required.',
    'unique'        => 'The :attribute field is required.',
    'min'           => 'The :attribute field is required.',
    'confirmed'     => 'The :attribute field is required.',
    'email'         => 'The :attribute field is required.',
    'alpha'         => 'The :attribute field is required.',
    'numeric'       => 'The :attribute field is required.',
    'boolean'       => 'The :attribute field is required.',
    'date_format'   => 'The :attribute field is required.',
    'alpha_num'     => 'The :attribute field is required.',
    'mimes'         => 'The :attribute field is required.',
);