<?php namespace NishNosh\User;
use View;
use Input;
use NishNosh\Admin\Repo\DataFormatter as AFormatter;
use Applum;
use FooterController;
class UserController
{
    public static function login()
    {
        View::make('site/head/head');

        View::make("site/user/login");
    }

    public static function postLogin()
    {
        $user = wp_signon( Input::all(), false );

        if ( is_wp_error($user) )
        {
            echo $user->get_error_message();
        }

        return header("Location:/profile");
    }

    public static function profile()
    {
        if(current_user_can('profile'))
        {
            $user_id = get_current_user_id();
            $usermeta = \UserMeta::where('user_id',$user_id)->first();
            $cities = self::idandname(\City::all(),"id","name");
            $provinces = self::idandname(\Province::all(),"id","name");
            $countries = self::idandname(\Country::all(),"id","name");
            View::make("site/user/profile",compact('cities','provinces','countries','usermeta'));
            die;
        }

        wp_redirect( "/login" );
        exit;
    }

    public static function postProfile()
    {
        if(current_user_can('profile'))
        {
            $user_id = get_current_user_id();

            $usermeta = \UserMeta::where('user_id',$user_id)->first();
            $usermeta->updateMe(\Input::all());
            self::profile();
        }
    }

    public static function shouldUpdateTheProfile()
    {
        if(isset($_POST['profile_submit']))
        {
            self::postProfile();
            return;
        }
    }

    public static  function idandname($collection,$idName,$nameColumn)
    {
        $temp = [];

        foreach($collection as $item)
        {
            $temp[$item->{$idName}] = $item->{$nameColumn};
        }

        return $temp;
    }

    public static function users()
    {

    }
}