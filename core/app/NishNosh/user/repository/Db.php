<?php namespace NishNosh\User\Repo;

use Wp\User\User;

/**
 * Class Db
 * @package NishNosh\User\Repo
 */
class Db
{
    /**
     * @param $id
     * @return bool|\WP_User
     */
    public function getRegisteredUser($id)
    {
        return User::findUserById($id);
    }
}