<?php namespace NishNosh\User\Repo;

/**
 * Class Formatter
 * @package NishNosh\User\Repo
 */
class Formatter
{
    /**
     * puts array into an object
     *
     * @param $array
     * @return \stdClass
     */
    public static function arrayToStdClass($array)
    {
        $std = new \stdClass();

        if(is_array($array))
            foreach ($array as $key => $value) {
                $std->{$key} = $value;
            }

        return $std;
    }

}