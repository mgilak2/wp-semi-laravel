<?php namespace NishNosh\User\Repo;

use Wp\Theme\Theme;

class Activation
{

    /**
    * Creates table applum with Theme::table
    *
    * @see Theme::table
    */
    public static function createTables()
    {
        Theme::table('applum-user-meta', function ($table) {
            $table->increments('id');

            $table->foreign('user_id')->references('ID')->on('wp-users');

            $table->string('city')->nullable();

            $table->string('province')->nullable();

            $table->string('country')->nullable();

            $table->integer('colonyCount')->nullable();

            $table->boolean('companyOrOther')->nullable();

            $table->boolean('beeKeepingOrder')->nullable();

            $table->integer('phone')->nullable();

            $table->integer('workExperience')->nullable();

            $table->integer('BeeKeepingBookletCode')->nullable();

            $table->boolean('BeeKeepingAsAMainJob')->nullable();

            $table->double('honeyHarvest')->nullable();

            $table->string('picture')->nullable();

            $table->string('education');

            $table->text('more');
        });
    }

}