<?php namespace NishNosh\User;

use NishNosh\User\Repo\Db;
use NishNosh\User\Repo\Formatter;
use NishNosh\User\Validation\FilterUserRegistrationInput;
use NishNosh\User\Validation\ValidateUserRegistration;
use Wp\Input\Input;
use Wp\User\User;
/**
 * Class Registration
 * @package NishNosh\User\Repo
 */
class Registration
{
    /**
     * @see NishNosh\User\Validation\ValidateUserRegistration
     * @var ValidateUserRegistration
     */
    protected $validator;

    /**
     * @see NishNosh\User\Validation\FilterUserRegistrationInput
     * @var FilterUserRegistrationInput
     */
    protected $filter;

    /**
     * NishNosh\User\Repo\Formatter
     * @var Formatter
     */
    protected $formatter;

    /**
     * NishNosh\User\Repo\Db
     * @var Db
     */
    protected $db;

    /**
     * @param ValidateUserRegistration $validator
     * @param FilterUserRegistrationInput $filter
     * @param Formatter $formatter
     * @param Db $db
     */
    public function __construct(ValidateUserRegistration $validator,
                                FilterUserRegistrationInput $filter,
                                Formatter $formatter,
                                Db $db)
    {
        $this->validator = $validator;
        $this->filter = $filter;
        $this->formatter = $formatter;
        $this->db = $db;
    }

    /**
     * sanitize the params .(if there was a problem in a occurred an exception will throw )
     *
     * @param array $wpNeeds
     * @return mixed returns user or errors
     */
    public function register(array $wpNeeds)
    {
        $user = new \stdClass();

        $user->user = null;
        $user->error = false;

        if( $error = $this->sanitize($wpNeeds) === true)
        {
            /**
             * making object from arrays for easier writability
             */
            $wpNeedsObj = $this->formatter->arrayToStdClass($wpNeeds);
            /**
             * doing the registration
             */
            $id = $this->insertUser($wpNeeds);

            /**
             * fetching registered user
             */
            if(is_integer($id))
            {
                $user = new \WP_User( $id );
                $user->add_cap("profile");

                $usermeta = new \UserMeta();
                $usermeta->user_id = $id;
                $usermeta->save();

                $usermeta = \UserMeta::where('user_id',$id)->first();

                $user = \WpUser::where("ID",$id)->first();
                $user->meta_id = $usermeta->id;
                $user->save();

                $user->user = $this->db->getRegisteredUser($id);

                unset($user->error);

                /**
                 * user structure is
                 * $user->user
                 * $user->email
                 */
                return $user;
            }

            return $user;
        }

        $user->error = $error;
        return $user;
    }

    /**
     * @param $wpNeedsObj
     *
     * @return mixed
     */
    private function insertUser($wpNeedsObj)
    {
        $id = User::register($wpNeedsObj);

        return $id;
    }

    /**
     * @param array $wpNeeds
     * @return bool
     */
    public function sanitize(array $wpNeeds)
    {
        $validation = $this->validator->check($wpNeeds);

        if ( ! $validation->fails() )
        {
            $this->filter->clean($wpNeeds);

            return true;
        }

        return $validation->messages();
    }

    public static function registorInstance()
    {
        return new self(
            new ValidateUserRegistration,
            new FilterUserRegistrationInput(new \GUMP()),
            new Formatter,
            new Db
        );
    }

    public static function shouldRegisterAUser()
    {
        if(Input::has("registerMeAUser"))
        {
            $registor = Registration::registorInstance();

            $user = $registor->register(Input::all());

            header('Content-Type: application/json');
            if(isset($user->error))
            {
                echo json_encode(['status'=>false]);
                die;
            }

            echo (json_encode(['status'=>true]));
            die;
        }

        return false;
    }
}