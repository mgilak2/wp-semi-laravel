<?php namespace NishNosh\User\Validation;

/**
 * Class FilterUserRegistrationInput
 * @package NishNosh\User\Validation
 */
class FilterUserRegistrationInput
{
    /**
     * @see \Gump
     * @var \GUMP
     */
    protected $gump;

    /**
     * @param \GUMP $gump
     */
    public function __construct(\GUMP $gump)
    {
        $this->gump = $gump;
    }

    public function clean(array $wpNeeds)
    {
        $wpNeeds = $this->gump->sanitize($wpNeeds);

        $this->gump->filter_rules(array(
            'username' => 'trim|sanitize_string',
            'password' => 'trim',
            'email'    => 'trim|sanitize_email'
        ));
    }
}