<?php namespace NishNosh\User\Validation;

use Wp\Validation\Validator;
use Wp\Input\Input;
class ValidateUserRegistration
{
    protected $messages = array(
        'required'      => 'The :attribute field is required.',
        'unique'        => 'The :attribute field is required.',
        'min'           => 'The :attribute field is required.',
        'confirmed'     => 'The :attribute field is required.',
        'email'         => 'The :attribute field is required.',
        'alpha'         => 'The :attribute field is required.',
        'numeric'       => 'The :attribute field is required.',
        'boolean'       => 'The :attribute field is required.',
        'date_format'   => 'The :attribute field is required.',
        'alpha_num'     => 'The :attribute field is required.',
        'mimes'         => 'The :attribute field is required.',
    );

    protected $rules = [
        'username' => 'required',
        'password' => 'required|min_len,8',
        'email' => 'required|valid_email',
//        'city' => 'alpha',
//        'province' => 'alpha',
//        'country' => 'alpha',
//        'colonyCount' => 'numeric',
//        'companyOrOther' => 'boolean',
//        'beeKeepingOrOther' => 'boolean',
//        'phone' => 'required,numeric',
//        'workExperience' => 'numeric',
//        'BeeKeepingBookletCode' => 'date_format:Y-m-d',
//        'BeeKeepingAsAMainJob' => 'boolean',
//        'honeyHarvest' => 'alpha_num',
//        'picture' => 'mimes:jpeg,gif,png',
//        'education' => 'alpha_num',
//        'more' => 'alpha_num',
    ];

    public function check()
    {
        $validator = Validator::make(
            Input::all(),
            $this->rules,
            $this->messages
        );

        return $validator;
    }
}