<?php namespace NishNosh\Admin;
use NishNosh\Admin\Repo\Activation as AActivation;
use NishNosh\Admin\Repo\Crop as ACrop;
use NishNosh\Admin\Repo\DataFormatter as AFormatter;
use NishNosh\Admin\Repo\Db as ADb;
use Wp\Theme\Theme,
    IndexController,
    Wp\View\View,
    Applum;

class Admin
{
    /**
     * prepare the data for view or model to get it updated
     *
     * @return \stdClass
     */
    public static function settingData()
    {
        if(isset($_POST['settingSubmitted']))
        {
            ACrop::cropOperation();

            ADb::update(ACrop::shouldFetchData());

            return AFormatter::renderForDataBinding(Applum::find(1));
        }

        else
        {
            return AFormatter::renderForDataBinding(Applum::find(1));
        }
    }



    /**
     * register the setting page and path settings values to the view
     */
    public static function setting()
    {
        Theme::setting(function(){
            $matched = self::settingData();

			$settings = AFormatter::objectiveSettings(Applum::find(1));

            View::make('admin/setting',['matched'=>$matched,'settings'=>$settings]);
        });
    }
    /**
     * used for making tables and seeding them
     */
	public static function activate()
	{
		AActivation::activate();
	}
}

