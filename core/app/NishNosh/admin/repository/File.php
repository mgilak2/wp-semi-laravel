<?php namespace NishNosh\Admin\Repo;

use Applum;

/**
 * extends \Wp\File\File for its functionality
 * Class File
 * @package NishNosh\Admin\Repo
 */
class File extends \Wp\File\File
{
    /**
     * iterates through $_FILES and extracts each item out of it
     * it uploads and pushes Apllum::$columns too
     * used for array based input intro[icon1] intro[icon2]
     *
     * @see File::fetchFile()
     * @see File::upload()
     */
    public static function updateFile()
    {
      foreach($_FILES as $column => $fileArray ) {
          foreach ( $fileArray['name'] as $key => $values ) {

              if ( $fileArray['size'][$key] > 0 ) {
                  $file = self::upload(self::fetchFile($column,$key));

                  $index = array_search($key,Applum::$match[$column]);

                  if($index !== false ) {
                      Applum::$columns[ $column ][ $key ] = $file->url;
                  }

              }
          }// end foreach file name array
      }//end foreach file
    }

    /**
     * downloads all filtered images base on their urls
     *
     * @param $baseAddress
     * @param $urls
     */
    public static function downloadAll($baseAddress,$urls)
    {
        foreach($urls as $url)
        {
            $name = self::get_file_name_with_its_url($url);

            self::downloadIt($baseAddress,$name,$url);
        }
    }

}