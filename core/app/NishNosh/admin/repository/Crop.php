<?php namespace NishNosh\Admin\Repo;

use Illuminate\Support\Facades\Config as Config;
use Wp\Input\Input,
    Applum,
    \Wp\Image\Image;
use NishNosh\Admin\Repo\Db as ADb,
    NishNosh\Admin\Repo\File as AFile;

/**
 * this class extends \Crop\Crop class for its functionality
 *
 * Class Crop
 * @package NishNosh\Admin\Repo
 */
class Crop extends \Crop\Crop
{
    /**
     * holds the result of blog api return
     * it is instantiated with null because of
     * ADb::update(self::$blogApiPosts) needs
     *
     * @var null
     */
    private static $blogApiPosts = null;

    /**
     * check whether gallery[fetchData] are checked or not
     * @see Input::get()
     * @return bool
     */
    public static function shouldFetchData()
    {
        return isset(Input::get('gallery')['fetchData'])
        && ! empty(Input::get('gallery')['fetchData']);
    }

    /**
     * used for gathering images which needs to crop
     *
     * @param $gallery
     * @return array
     */
    public static function gatherImagesForCrop($gallery)
    {
        $images = array();

        foreach ( Applum::$coords as $coord ) {
            if(isset(Input::get('gallery')[$coord]) && ! empty(Input::get('gallery')[$coord]))
            {
                $number = substr($coord,-1,2);

                $images[] = [
                    'url'=>$gallery['featureImage'.$number],
                    'coords'=>json_decode(stripcslashes(Input::get('gallery')[$coord]))
                ];
            }
        }

        return $images;
    }

    /**
     * @return bool checks or
     */
    public static function shouldCrop()
    {
        $crop = false;

        foreach ( Applum::$coords as $coord ) {
            if(isset(Input::get('gallery')[$coord]) && ! empty(Input::get('gallery')[$coord]))
            {
                $crop = true;
                break;
            }
        }

        return $crop;
    }
    
    public static function cropAll(array $images)
    {
        foreach ($images as $image) {

            $image = Utilities::arrayToObject($image);
            $image->name = File::get_file_name_with_its_url($image->url);
            $image->mimeType = Image::mimeType($image->name);
            $image->path = \Crop\Crop::$direction."/images/".$image->name;
            $image->savePath = \Crop\Crop::$direction."/croped/"."croped-".$image->name;

            \Crop\Crop::cutIt($image);
        }

    }

    /**
     * bundles whole crop actions
     * checks for fetching data
     * and saving it into database
     * and also checks for crop action
     * and doing the crop
     */
    public static function cropOperation()
    {
        if(self::shouldFetchData())
        {
            $ids = Input::get("gallery")['ids'];

            $ids = explode(",",$ids);

            self::$blogApiPosts = Api::useBlogApi($ids);

            ADb::update_crops_post_info_by_blogApi(self::$blogApiPosts);

            $images = array();

            foreach (self::$blogApiPosts as $post) {
                $images[] = $post->featured;
            }

            AFile::downloadAll(\Crop\Crop::$direction."/images/",$images);
        }

        if(self::shouldCrop())
        {
            $settings = Applum::find(1);
            $gallery = $settings->gallery;

            $images = self::gatherImagesForCrop(unserialize($gallery));

            self::cropAll($images);
        }
    }

    /**
     * generates cropped image link
     *
     * @param $url
     * @return string
     */
    public static function show($url)
    {
        $name = AFile::get_file_name_with_its_url($url);

        return \App\Config\Config::theme()
        . "/core/app/helpers/Crop/croped/croped-"
        .  $name;
    }
}