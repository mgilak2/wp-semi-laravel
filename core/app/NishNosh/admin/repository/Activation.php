<?php namespace NishNosh\Admin\Repo;
use Wp\Database\Database;
use Wp\Theme\Theme,Applum;

/**
 * creates applum-setting table and seeds it
 *
 * Class Activation
 * @package NishNosh\Admin\Repo
 */
class Activation
{
    /**
     * Creates table applum with Theme::table
     *
     * @see Theme::table
     */
    public static function createTables()
    {
        Theme::create('applum-setting', function ($table)
        {
            $table->increments('id');

            $table->text('intro')->nullable();

            $table->text('feature')->nullable();

            $table->text('app')->nullable();

            $table->text('subscribe')->nullable();

            $table->text('gallery')->nullable();

            $table->text('testimonial')->nullable();

            $table->text('contact')->nullable();

            $table->text('pricing')->nullable();

            $table->text('footer')->nullable();

        });

        Theme::create("usermetas",function($table)
        {
            $table->string("name");

            $table->string("lastName");

            $table->integer('city_id');

            $table->integer('country_id');

            $table->integer('county_id');

            $table->integer("hive_count");

            $table->boolean("cooperative_member");

            $table->boolean("union_beekeeper_member");

            $table->integer("phone");

            $table->string("email");

            $table->integer("month_record");

            $table->integer("year_record");

            $table->integer("beekeeping_book_code");

            $table->boolean("beekeeping_as_main_job");

            $table->integer("amount_of_honey_extraction_per_year");

            $table->integer("education");

            $table->string("picture");

            $table->text("description");
        });

        Theme::create("baseInfo",function($table)
        {
            $table->increments("id");

            $table->integer("type");

            $table->string("title");

            $table->integer("parent");
        });

        Theme::create("user_meta",function($table)
        {
            $table->increments("id");

            $table->integer("user");

            $table->integer("meta");
        });
    }

    /**
     * seeds applum-setting table
     * with the use of array and serialization
     * each fields of applum-setting are filled with
     * serialized array
     */
    public static function seeds()
    {
        Theme::seed('Applum', null, function () {

            $applum = new Applum;

            $intro['subject'] = "عنوان  مقدمه";
            $intro['content'] = "متن مقدمه";
            $intro['contentButton'] = "دکمه مقدمه";
            $intro['signupSubject'] = "عنوان عضویت";
            $intro['signupContent'] = "متن عضویت";
            $intro['signupButton'] = "دکمه عضویت";
            $intro['iphone'] = asset('images/devices/head-iphone.png');
            $intro['network'] = asset('images/head_icons.png');
            $intro['background'] = asset('images/backgrounds/head.jpg');
            $intro['icon1'] = asset('images/stores/appstore.png');
            $intro['icon2'] = asset('images/stores/google.png');
            $intro['icon3'] = asset('images/stores/windows.png');

            $applum->intro = serialize($intro);

            $feature['subject'] = 'عنوان ویژگی ها';
            $feature['subject1'] = 'عنوان ویژگی ها اول';
            $feature['content1'] = 'متن ویژگی های اول';
            $feature['subject2'] = 'عنوان ویژگی های دوم';
            $feature['content2'] = 'متن ویژگی های دوم';
            $feature['subject3'] = 'عنوان ویژگی های سوم';
            $feature['content3'] = 'متن ویژگی های سوم';
            $feature['subject4'] = 'عنوان ویژگی های چهارم';
            $feature['content4'] = 'متن ویژگی های چهارم';
            $feature['subject5'] = 'عنوان ویژگی های پنجم';
            $feature['content5'] = 'متن ویژگی های پنجم';
            $feature['subject6'] = 'عنوان ویژگی های ششم';
            $feature['content6'] = 'متن ویژگی های ششم';
            $feature['subject7'] = 'عنوان ویژگی های هفتم';
            $feature['content7'] = 'متن ویژگی های هفتم';
            $feature['subject8'] = 'عنوان ویژگی های هشتم';
            $feature['content8'] = 'متن ویژگی های هشتم';

            $applum->feature = serialize($feature);

            $app['subject'] = 'عنوان اپلیکیشن';
            $app['content'] = 'متن اپلیکیشن';
            $app['button1'] = 'دکمه ی اول';
            $app['button2'] = 'دکمه ی دوم';
            $app['tablet'] = asset('images/devices/ipad.png');
            $app['iphone'] = asset('images/devices/iphone.png');
            $app['background'] = asset('images/geometric.jpg');

            $applum->app = serialize($app);

            $subscribe['subject'] = 'عنوان عضویت';
            $subscribe['content'] = 'متن عضویت';
            $subscribe['button'] = 'دکمه ی عضویت';
            $subscribe['background'] = asset('images/backgrounds/subscribe.jpg');

            $applum->subscribe = serialize($subscribe);

            $gallery['subject'] = 'عنوان مطالب اخیر';
            $gallery['countPost'] = 6;

            $applum->gallery = serialize($gallery);

            $testimonial['subject'] = 'عنوان بخش ';
            $testimonial['count'] = 6;
            $testimonial['background'] = asset('images/backgrounds/testimonials.jpg');

            $applum->testimonial = serialize($testimonial);

            $contact['subject'] = 'عنوان بخش تماس با ما';
            $contact['content'] = 'متن بخش تماس با ما';
            $contact['button'] = 'دکمه ی تماس با ما';

            $applum->contact = serialize($contact);

            $footer['button1'] = 'دکمه ی اول';
            $footer['button2'] = 'دکمه ی دوم';
            $footer['button3'] = 'دکمه ی سوم';
            $footer['twitter'] = 'توییتر';
            $footer['facebook'] = 'فیس بوک';
            $footer['plus'] = 'گوگل پلاس';
            $footer['icon1'] = asset('images/stores/appstore.png');
            $footer['icon2'] = asset('images/stores/google.png');
            $footer['icon3'] = asset('images/stores/windows.png');

            $applum->footer = serialize($footer);

            $applum->save();
        });

    }

    /**
     * creates applum-setting table and seeds it
     */
    public static function activate()
    {
        self::createTables();

        self::seeds();
    }

}