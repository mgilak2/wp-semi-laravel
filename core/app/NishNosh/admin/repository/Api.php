<?php namespace NishNosh\Admin\Repo;
use BlogApi,App\Config\Config;
class Api
{
    public static function useBlogApi(array $ids)
    {
        $posts = BlogApi::grabPosts(Config::apiPath(),[
            'ids'=>serialize($ids),
            'userPass'=> Config::apiPass()
        ]);

        return $posts;
    }
}