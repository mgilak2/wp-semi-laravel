<?php namespace NishNosh\Admin\Repo;
use NishNosh\Admin\Repo\File as AFile;
use NishNosh\Admin\Repo\DataFormatter as ADataFormatter;
use NishNosh\Admin\Repo\Activation as AActivation;
use Applum;
/**
 * this class bundles the whole repository classes into itself
 * Class Utilities
 * @package NishNosh\Admin\Repo
 */
class Utilities
{
    public static function arrayToObject(array $array)
    {
        $std = new \stdClass();

        foreach($array as $key => $val)
        {
            $std->{$key} = $val;
        }

        return $std;
    }
}