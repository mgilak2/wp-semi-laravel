<?php namespace NishNosh\Admin\Repo;
use Applum;

/**
 * this class stands between the submitted data to the host or sent data to the view
 * and its strategy can be use in many project
 * i make this a little bit more usable in more time
 *
 * Class DataFormatter
 * @package NishNosh\Admin\Repo
 */
class DataFormatter
{
    /**
     * with the help of Applum::$columns
     * and the similarity between input names with it
     * we can loop through input and fill the values of them into
     * an array . $key holds a column name . and $input is the name of
     * input and fields in db and $val is equal to the settings that the user
     * sent
     *
     * Applum::$columns are filled with the name of db column
     * each db column are an array
     * at the end we serialize arrays and save it into their columns
     *
     * @param $settings
     * @return \stdClass
     */
    public static function renderForDataBinding(&$settings)
    {
        $std = new \stdClass();

        foreach ( Applum::$columns as $key => $val )
        {
            $column = unserialize($settings->{$key});

            if(is_array($column))
            {
                foreach($column as $input => $value)
                {
                    $std->{$key.'['.$input.']'} = $value;
                }
            }
        }// end foreach Applum::$column

        return $std;
    }//end renderForDataBinding

    /**
     * with the help of Applum::$match
     * and its similarity with input names
     * we can loop through input and fill the values of them into
     * an array . $key holds a column name index . and $input is the name of
     * input and fields in db and $val is equal to the settings that the user
     * sent
     *
     * Applum::$columns are filled with the name of db column
     * each db column are an array
     * at the end we serialize arrays and save it into their columns
     *
     */
    public static function render($blogApi = false)
    {
        foreach($_POST as $key => $val)
        {
            if(is_array($val))
            {
                foreach ( $val as $input => $value )
                {
                    /**
                     * i check for existance of input inside our defined database filled in
                     * Applum::$match
                     */
                    $index = array_search($input,Applum::$match[$key]);

                    if($index !== false )
                    {
                        if($blogApi and $key != "gallery")
                        {
                            Applum::$columns[$key][$input] = $value;
                        }
                    }
                }// end foreach
            }// end is array if

        }// end post foreach

    }// render

    /**
     * this function update the existing ones
     * or add more option to the array
     *
     * @param $current
     * @param $proccesed
     * @return mixed
     */
    public static function merge($current,$proccesed)
    {
        foreach ( $current as $key => $val )
        {
            if(isset($proccesed[$key]))
            {
                $current[$key] = $proccesed[$key];
            }
        }

        foreach ($proccesed as $key => $value)
        {
            if(! isset($current[$key]))
            {
                $current[$key] = $proccesed[$key];
            }
        }

        return $current;
    }

    public static function arrayToStdClass($array)
    {
        $std = new \stdClass();

        if(is_array($array))
            foreach ($array as $key => $value) {
                $std->{$key} = $value;
            }

        return $std;
    }

    public static function objectiveSettings($data)
    {
        $std = new \stdClass();

        foreach(Applum::$columns as $column => $items)
        {
            $std->{$column} = self::arrayToStdClass(unserialize($data->{$column}));
        }

        return $std;
    }
}