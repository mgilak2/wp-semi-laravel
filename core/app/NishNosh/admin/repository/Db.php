<?php namespace NishNosh\Admin\Repo;
use Applum;
use NishNosh\Admin\Repo\File as AFile;

/**
 *
 * Class Db
 * @package NishNosh\Admin\Repo
 */
class Db
{

    /**
     * @var object $settings use for singleton and global access upon settings info
     */
    public static $settings = null;

    /**
     * updates the whole setting section
     * firsts it renders the input names of the submitted form
     * into database fields
     * it also merge the current settings or update the submitted settings into
     * db .
     * it also use serialization for each fields
     *
     * @param boolean $blogApi
     */
	public static function update($blogApi = false)
	{
        /**
         * set Self::$settings with Applum::find(1)
         */
		self::setSettings();

        /**
         * renders submitted form inputs
         * which is compatible with the database structure
         *
         * IMPORTANT : I have to fine a better place for this one
         */
        DataFormatter::render($blogApi);

        /**
         * updates uploaded file info into database
         */
		AFile::updateFile();

        /**
         * for the sake of serialization there is some boundaries . we can`t
         * change new settings with the current one , we have to maintain the untouched
         * settings and update the touched ones .
         */
        self::use_submitted_info_and_leave_untouch_settings();

		self::$settings->save();
	}

    /**
     * its name is clear about what it is performing for you
     */
    public static function use_submitted_info_and_leave_untouch_settings()
    {
        foreach ( Applum::$columns as $key => $val )
        {
            $currentSetting = unserialize(self::$settings->{$key});

            if(is_array($currentSetting))
            {
                $processed = DataFormatter::merge($currentSetting,Applum::$columns[$key]);

                self::$settings->{$key} = serialize($processed);
            }
        }
    }

    /**
     * updating gallery settings > posts section info
     * with result of Blog Api
     * in fact this will be called only if the user checked the gallery[fetchData]
     * checkbox
     *
     * @param array $posts provided by BlogApi result
     */
    public static function update_crops_post_info_by_blogApi($posts)
    {
        Applum::
        update_gallery_with_blog_api_result_for_crops($posts);
    }

    /**
     * sets the settings (singleton and globalization)
     */
    public static function setSettings()
    {
        if(self::$settings == null)
            self::$settings = Applum::find(1);
    }
}