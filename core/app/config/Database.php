<?php
/**
 * holds database info in eloquent format
 */
return array(
    		'driver'    => 'mysql',
			'host'      => '127.0.0.1',
			'database'  => 'wpeloquent',
			'username'  => 'root',
			'password'  => '',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
);