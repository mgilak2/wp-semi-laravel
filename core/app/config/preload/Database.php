<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection( \Config::db() );

$capsule->setAsGlobal();

$capsule->bootEloquent();