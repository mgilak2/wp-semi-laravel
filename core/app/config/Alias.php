<?php
/**
 * returns aliases and full classes
 * each item of this array will be used in class_alias function
 */
return array(
    "Form"         =>  "\\Form\\Form",
    "Blade"        =>  "\\Philo\\Blade\\Blade",
    "View"         =>  "\\Wp\\View\\View",
    "Config"       =>  "\\App\\Config\\Config",
    "Eloquent"     =>  "\\Illuminate\\Database\\Eloquent\\Model",
    "Menu"         =>  "\\Wp\\Menu\\Menu",
    "Theme"        =>  "\\Wp\\Theme\\Theme",
    "Database"     =>  "\\Wp\\Database\\Database",
    "Session"      =>  "\\Wp\\Session\\Session",
    "Action"       =>  "\\Wp\\Action\\Action", 
    "Input"        =>  "\\Wp\\Input\\Input",
    "File"         =>  "\\Wp\\File\\File",
    "Api"          =>  "\\Wp\\Api\\Api",
    "Post"         =>  "\\Wp\\Post\\Post",
    "Crop"         =>  "\\Crop\\Crop",
    "Image"        =>  "\\Wp\\Image\\Image",
    "Auth"         =>  "\\Wp\\Auth\\Auth",
    "User"         =>  "\\Wp\\User\\User",
    "Cookie"       =>  "\\Wp\\Cookie\\Cookie",
    "Validator"    =>  "\\Wp\\Validation\\Validator",
    "Ajax"         =>  "\\Wp\\Ajax\\Ajax",
);