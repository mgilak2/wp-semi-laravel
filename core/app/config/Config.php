<?php namespace App\Config;
/**
 * its a wrapper around website configuration
 * you can add any functions or variables to it
 *
 * Class Config
 * @package App\Config
 */
class Config
{

    public static function salt()
    {
        return "d3991dk^sSK*JsdVX7/.65KSNC234";
    }

    /**
     * returns theme direction / core / view
     * @return string
     */
    public static function view()
    {
        return __DIR__."/../views";
    }

    /**
     * returns theme direction / core / cache
     * @return string
     */
    public static function bladeCache()
    {
        return __DIR__."/../cache";
    }

    /**
     * returns database info in eloquent format
     * @return array
     */
    public static function db()
    {
        return array(
    		'driver'    => 'mysql',
			'host'      => DB_HOST,
			'database'  => DB_NAME,
			'username'  => DB_USER,
			'password'  => DB_PASSWORD,
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
        );   
    }

    /**
     * used for singleton pattern
     * @var null
     */
    public static $theme = null;

    /**
     * fill self::$theme with the singleton pattern and returns it
     * @return null|string
     */
    public static function theme()
    {
        if( self::$theme == null )
        {
            self::$theme = get_template_directory_uri();   
            return self::$theme;   
        }   
        
        else
        {
            return self::$theme;   
        }
    }

	public static function apiPass()
	{
		return "dadavC564n/VD657S*^&KLi(6_(e234hfivISJBN";
	}

	public static function apiPath()
	{
		return "http://dev/wp-4/";
	}
  
}