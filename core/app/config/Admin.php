<?php 

//class AdminConfig implements iAdminConfig
//{
//
//
//    public static intro()
//    {
//
//
//
//    }
//
//
//}


// Default options values
$sa_options = array(
    'footer_copyright' => '&copy; ' . date('Y') . ' ' . get_bloginfo('name'),
    'intro_text' => '',

    'farmandar-pic' => "http://nish:8080/wp-content/themes/nish-o-nosh-responsive/img/videocamera.png",
    'farmandar-speech' => "سلامتی، سرزندگی و شادابی و امیدواریم سال 1393 سال رونق در عرصه اقتصادی، اعتدال در عرصه سیاسی و شور نشاط در عرصه اجتماعی برای مردم مازندران باشد. (رضا گنجی جویباری فرماندارشهرستان جویبار)",

    'monasebat-pic' => "http://nish:8080/wp-content/themes/nish-o-nosh-responsive/img/videocamera.png",
    'monasebat-text' =>"سلامتی، سرزندگی و شادابی و امیدواریم سال 1393 سال رونق در عرصه اقتصادی، اعتدال در عرصه سیاسی ",

    'pic1' => 'http://nish:8080/wp-content/themes/nish-o-nosh-responsive/img/videocamera.png',
    'pic2' => "http://nish:8080/wp-content/themes/nish-o-nosh-responsive/img/videocamera.png",
    'pic3' => "http://nish:8080/wp-content/themes/nish-o-nosh-responsive/img/videocamera.png",
    'pic4' => "http://nish:8080/wp-content/themes/nish-o-nosh-responsive/img/videocamera.png",

    'bazdid1' => "",
    'bazdid1-text' => "",
    'bazdid2-text' => "",
    'bazdid2' => "",
    'bazdid3-text' => "",
    'bazdid3' => "",
    'bazdid4-text' => "",
    'bazdid4' => "",
    'bazdid5-text' => "",
    'bazdid5' => "",
    'bazdid6-text' => "",
    'bazdid6' => "",

    "payamak" => "",
    "payamak-link"=>"",
    "payamak-text" => "",


    "sokhan"=>
    array(
    'sokhan[1][text]' => 'نشان منافق سه چيز است : 1 - سخن به دروغ بگويد . 2 - از وعده تخلف كند .3 - در امانت خيانت نمايد . ',
    'sokhan[1][pic]' => ' ',

        'sokhan[2][text]' => 'اگر از کارنیک خود خوشحال واز کار بد خود دلگیر میشوی مومن هستی.  ',
        'sokhan[2][pic]' => ' ',

        'sokhan[3][text]' => 'مومن ازهمه مردم گرفتارتراست،زیرابایدبکاردنیاوآخرت هردو برسد. ',
        'sokhan[3][pic]' => ' ',

    'sokhan[4][text]' => 'کسیکه مومن درمانده وبی بناهی را بناه دهد،روز قیامت دربناه خداخواهد بود. ',
        'sokhan[4][pic]' => '',

    'sokhan[5][text]' => 'من وآنکس که سربرستی یتیمی رابعهده بگیرد،دربهشت ماننددوانگشت بهلوی هم قرار داریم. ',
        'sokhan[5][pic]' => '',

    'sokhan[6][text]' => 'محبوبترین خانه های شمادرنظر خدا،خانه ایست که درآن یتیمی محترم باشد. ',
        'sokhan[6][pic]' => '',

    'sokhan[7][text]' => 'از آنچه نمیخواهی درباره تو بگویند برکنار باش ',
        'sokhan[7][pic]' => '',

    'sokhan[8][text]' => 'کارخیر را زود انجام ده تاکامیاب شوی. ',
        'sokhan[8][pic]' => '',

    'sokhan[9][text]' => 'چشم بمال مردم نداشته باش تادردوستی تو بکوشند. ',
        'sokhan[9][pic]' => '',

    'sokhan[10][text]' => 'بهترین رفیق آنست که بکار خیر رهبریت کند. ',
        'sokhan[10][pic]' => '',

    'sokhan[11][text]' => '.آسایش گرسنه چشم ازهرکس کمتر باشد. ',
        'sokhan[11][pic]' => '',

    'sokhan[12][text]' => 'علم گنج،بزرگی است که باخرج کردن تمام نمیشود ',
        'sokhan[12][pic]' => '',

    'sokhan[13][text]' => 'زبان،حیوان درنده است،اگر رها شود می گزد ',
        'sokhan[13][pic]' => '',

    'sokhan[14][text]' => 'قلب خودراازکینه دیگران باک کن،تاقلب آنها ازکینه تو باک شود ',
        'sokhan[14][pic]' => '',

    )
);
