<?php
use Wp\View\View;

/**
 * Class GalleryController
 */
class GalleryController
{
    
    public static function gallery($data)
    {
        View::make('site/body/gallery/gallery',['gallery'=>$data->gallery]);
    }
    
}