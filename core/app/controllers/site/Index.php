<?php
use NishNosh\Admin\Repo\DataFormatter as AFormatter;
class IndexController
{
    public static function display()
    {
        $data = AFormatter::objectiveSettings(Applum::find(1));

        View::make('site/head/head');
        
        View::make("site/body/header/header");

        IntroController::intro($data);
        
        FeatureController::feature($data);
        
        AppController::app($data);
        
        SubscribeController::subscribe($data);
        
        GalleryController::gallery($data);
        
        TestimonialController::testimonial($data);
        
        PricingController::pricing($data);
        
        ContactController::contact($data);
        
        FooterController::footer($data);
    }
    
}