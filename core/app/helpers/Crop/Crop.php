<?php namespace Crop;
use Wp\Image\Image;

/**
 * Crops png and jpeg/jpg which exist on your host
 * i`m using jcrop code
 *
 * Class Crop
 * @package Crop
 */
Class Crop
{

    public static $direction = __DIR__;

    /**
     * cuts the given png or jpeg picture with coordination and its full path on host server
     *
     * @param object $img
     * $img->name contains name of resource/image
     * $img->mimeType contains mime type of resource/image
     * $img->coords its a object which holds the beginnings of the crop section
     * and height and width of the crop section within itself
     * $img->coords->x1 , $img->coords->y1 , $img->coords->h , $img->coords->w
     */
	public static function cutIt($img)
	{
        /**
         * Image::saveImage dependencies
         */
        $img_quality = 90;

        /**
         * loads a resource from host
         * @see Image::createImage
         */
        $img_r = Image::createImage($img->mimeType,$img->path);
        $img->quality = $img_quality;

        /**
         * creating an image in ram to put our cropped image into it
         */
		$dst_r = ImageCreateTrueColor( $img->coords->w, $img->coords->h );
        $img->imgRes = $dst_r;

        /**
         * doing crop in here using $coords , $img_r ,$dst_r and ...
         */
		imagecopyresampled($dst_r,$img_r,0,0,$img->coords->x1,$img->coords->y1,
            $img->coords->w,$img->coords->h,$img->coords->w,$img->coords->h);

        /**
         * saving the result to the host
         */
        Image::saveImage($img);
	}

}