<?php
/**
 * Class Applum
 */
class Country extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'country';
    public $timestamps = false;
}