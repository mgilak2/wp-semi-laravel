<?php
/**
 * Class Applum
 */
class Province extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'province';
    public $timestamps = false;
}