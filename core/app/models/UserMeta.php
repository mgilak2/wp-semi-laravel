<?php

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    protected $table = 'usermetas';
    public $timestamps = false;

    public function updateMe($inputs)
    {
        $this->name                   = $inputs['name'];
        $this->lastName               = $inputs['lastName'];
        $this->city_id                = $inputs['city_id'];
        $this->country_id             = $inputs['country_id'];
        $this->province_id            = $inputs['province_id'];
        $this->village             = $inputs['village'];
        $this->hive_count             = $inputs['hive_count'];
        $this->cooperative_member     = (isset($inputs['cooperative_member']))?$inputs['cooperative_member']:0;
        $this->union_beekeeper_member = (isset($inputs['union_beekeeper_member']))?$inputs['union_beekeeper_member']:0;
        $this->phone                  = $inputs['phone'];
        $this->email                  = $inputs['email'];
//        $this->month_record           = $inputs[''];
        $this->year_record            = $inputs['year_record'];
        $this->beekeeping_book_code   = $inputs['beekeeping_book_code'];
        $this->post_code              = $inputs['post_code'];
        $this->beekeeping_as_main_job = (isset($inputs['beekeeping_as_main_job']))?$inputs['beekeeping_as_main_job']:0;
        $this->amount_of_honey_extraction_per_year = $inputs[''];
        $this->education_id           = $inputs['education_id'];
//        $this->picture                = $inputs[''];
        $this->description            = $inputs['description'];
        $this->cellphone              = $inputs['cellphone'];
        $this->union_name             = $inputs['union_name'];
        $this->job_id                 = $inputs['job_id'];
        $this->location               = $inputs['location'];
        $this->practice               = serialize($inputs['practice']);
        $this->picture                = (isset($_FILES['picture']))?\File::upload($_FILES['picture'])->url:null;

        $this->save();
    }

    public function user()
    {
        return $this->hasOne("User","ID","id");
    }
}