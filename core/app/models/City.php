<?php
/**
 * Class Applum
 */
class City extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'city';
    public $timestamps = false;
}