<?php
/**
 * Class Applum
 */
class Applum extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'applum-setting';

    public $timestamps = false;

    /**
     * @param array $posts results by blog api
     */
    public static function update_gallery_with_blog_api_result_for_crops($posts)
    {
        self::truncateCrops();

        self::setCrops($posts);
    }

    /**
     * makes all crops information empty
     */
    public static function truncateCrops()
    {
        foreach (self::$cropArray as $cropRow)
        {
            foreach($cropRow as $cropItem)
            {
                self::$columns['gallery'][$cropItem] = null;
            }
        }
    }

    /**
     * @param array $posts provided with blog api result
     * and each row of it contain an stdClass object
     * with post_title , post_content , featured (feature image)
     */
    public static function setCrops($posts)
    {
        for ($i=0;$i<count($posts);$i++)
        {
            self::$columns['gallery'][self::$cropArray[$i][0]] = $posts[$i]->post_title;
            self::$columns['gallery'][self::$cropArray[$i][1]] = $posts[$i]->post_content;
            self::$columns['gallery'][self::$cropArray[$i][2]] = $posts[$i]->featured;
        }
    }

    /**
     * @var array used for updating database with blog api
     */
    public static $cropArray = [
        ['cropSubject1','cropContent1','featureImage1'],
        ['cropSubject2','cropContent2','featureImage2'],
        ['cropSubject3','CropContent3','featureImage3'],
        ['cropSubject4','cropContent4','featureImage4'],
        ['cropSubject5','cropContent5','featureImage5'],
        ['cropSubject6','cropContent6','featureImage6']
    ];

    /**
     * @var array used for updating crop data when the user crop
     * and save his settings pictures
     */
    public static $coords = [
        'coords1','coords2','coords3',
        'coords4','coords5','coords6'
    ];

    /**
     * @var array used for updating database based on blog api
     */
    public static $cropImages = [
        'featureImage1',
        'featureImage2',
        'featureImage3',
        'featureImage4',
        'featureImage5',
        'featureImage6'
    ];

    /**
     * @var array which used for bootstraping all submitted data to its defined array
     * and also used for serialization and saving data into db with eloquent
     */
	public static $columns = [
		'intro'=>array(),
		'feature'=>array(),
		'app'=>array(),
		'subscribe'=>array(),
		'gallery'=>array(),
		'testimonial'=>array(),
		'contact'=>array(),
		'footer'=>array(),
        'pricing'=>array()
	];

    /**
     * @var array used for mass assignment
     * and bootstraps submitted data into their defined array/db column
     *
     * IMPORTANT :: if must add some other fields this array must be tampered with
     */
    public static $match = array(
        'intro' => [
	        "subject",
            "content",
            "contentButton",
            'signupSubject',
            'signupContent',
            'signupButton',
	        'background',
	        'network',
	        'iphone',
	        'icon3',
	        'icon2',
	        'icon1'
		],
	    'feature' => [
	        'subject',
	        'subject1','content1',
	        'subject2','content2',
	        'subject3','content3',
	        'subject4','content4',
	        'subject5','content5',
	        'subject6','content6',
	        'subject7','content7',
	        'subject8','content8',
		],
        'app' => [
	        'subject',
            'content',
            'button1',
            'button2',
            'tablet',
            'iphone',
            'background'
		],

        'subscribe'=>[
	        'subject',
		    'content',
		    'button',
            'background'
        ],

	    'gallery' => [
            'background',
		    'subject',
		    'fetchData',
			'ids',
		    'cropSubject1','cropContent1','featureImage1','cropImage1',
		    'cropSubject2','cropContent2','featureImage2','cropImage2',
		    'cropSubject3', 'CropContent3', 'featureImage3', 'cropImage3',
		    'cropSubject4','cropContent4','featureImage4','cropImage4',
		    'cropSubject5','cropContent5','featureImage5','cropImage5',
		    'cropSubject6', 'cropContent6', 'featureImage6', 'cropImage6'
		],

        'testimonial' => [
		    'subject',
	        'count',
	    ],

        'contact' => [
	        'subject',
	        'content',
	        'button'
		],
        'footer' => [
	        'button1',
            'button2',
            'button3',
            'twitter',
            'facebook',
            'plus',
            'icon1',
            'icon2',
            'icon3'
        ],
    );


}