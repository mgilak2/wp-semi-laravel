<?php

class Hasan extends Eloquent
{
 
    protected $fillable = array('title');
    
    protected $table = 'hasan';
    
    public $timestamps = false;
}