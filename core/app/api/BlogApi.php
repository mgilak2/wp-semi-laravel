<?php

/**
 * used nish-o-nosh service over its api
 * and it extends \Wp\Api\Api for its functionality
 *
 * Class BlogApi
 */
class BlogApi extends \Wp\Api\Api
{
    /**
     * its grabs the serialized answer from web service
     * and returns back Unserialized
     *
     * @param string $address
     * @param array $args
     * @return string $result
     */
    public static function grabPosts($address ,$args = array())
    {
        $respond = self::deploy($address,$args);

        $result = unserialize($respond);

        return $result;
    }

    /**
     * @var array $columns fields of crops
     */
	public static $columns = [
		[
			'galleryCropSubject1',
			'galleryCropContent1',
			'Coords1'
		],
		[
			'galleryCropSubject2',
			'galleryCropContent2',
			'Coords2'
		],
		[
			'galleryCropSubject3',
			'galleryCropContent3',
			'Coords3'
		],
		[
			'galleryCropSubject4',
			'galleryCropContent4',
			'Coords4'
		],
		[
			'galleryCropSubject5',
			'galleryCropContent5',
			'Coords5'
		],
		[
			'galleryCropSubject6',
			'galleryCropContent6',
			'Coords6'
		]
	];
}